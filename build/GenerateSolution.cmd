@ECHO OFF
ECHO Generating solution files...
IF EXIST "x86_64" RMDIR /S /Q "x86_64"
MKDIR "x86_64"
PUSHD "x86_64"
cmake ..\.. -G "Visual Studio 16 2019" -DCMAKE_PREFIX_PATH="C:\Qt\5.15.0\msvc2019_64"
POPD
POPD
ECHO Solution files generated.
PAUSE