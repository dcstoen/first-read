#include <boost/test/unit_test.hpp>
#include <QFileInfo>
#include <QFile>

#include "Fixtures/ScreenplayFixture.h"

namespace FirstRead
{

BOOST_AUTO_TEST_SUITE(ScreenplayTests)

/// @brief Fixture for testing QTextDocument styling construction from QDomDocument.
struct ConstructStylingsFixture
{
   /// @brief Constructor
   ConstructStylingsFixture()
      : UUT_()
      , dom_()
   {

   }

   /// @brief Setup dom to test.
   /// @param fileName The file to use for populating the dom.
   /// @param x The value to use for dpiX.
   /// @param y The value to use for dpiY.
   void Setup(QString fileName, int x, int y)
   {
      UUT_.dpiX_ = x;
      UUT_.dpiY_ = y;

      QFileInfo fileInfo(fileName);
      if (fileInfo.exists() && fileInfo.isFile())
      {
         QFile file(fileInfo.filePath());
         BOOST_REQUIRE(file.open(QIODevice::ReadOnly));
         dom_.setContent(&file);
         file.close();
      }
   }

   /// @brief Constructs stylings from dom.
   void Run()
   {
      UUT_.ConstructStylings(dom_);
   }

   /// @brief Verifies a style to the given values.
   /// @param styleName The name of the style to verify.
   /// @param size The expected size.
   /// @param caps The expected caps value.
   /// @param bold The expected bold flag.
   /// @param italic The expected italics flag.
   /// @param underline The expected underline flag.
   /// @param topMargin The expected top margin.
   /// @param bottomMargin The expected bottom margin.
   /// @param leftMargin The expected left margin.
   /// @param rightMargin The expected right margin.
   /// @param align The expected text alignment.
   void VerifyStyling(QString styleName, 
      double size, 
      QFont::Capitalization caps,
      bool bold,
      bool italic, 
      bool underline,
      double topMargin,
      double bottomMargin,
      double leftMargin,
      double rightMargin,
      Qt::AlignmentFlag align)
   {
      auto iter = UUT_.styleTypes_.find(styleName);
      BOOST_REQUIRE(iter != UUT_.styleTypes_.end());

      BOOST_CHECK_EQUAL(iter->second.first.font().pointSizeF(), size);
      BOOST_CHECK_EQUAL(iter->second.first.font().capitalization(), caps);
      BOOST_CHECK_EQUAL(iter->second.first.font().bold(), bold);
      BOOST_CHECK_EQUAL(iter->second.first.font().italic(), italic);
      BOOST_CHECK_EQUAL(iter->second.first.font().underline(), underline);
      BOOST_CHECK_EQUAL(iter->second.second.topMargin(), topMargin);
      BOOST_CHECK_EQUAL(iter->second.second.bottomMargin(), bottomMargin);
      BOOST_CHECK_EQUAL(iter->second.second.leftMargin(), leftMargin);
      BOOST_CHECK_EQUAL(iter->second.second.rightMargin(), rightMargin);
      BOOST_CHECK_EQUAL(iter->second.second.alignment(), align);
   }

   ScreenplayFixture UUT_; ///< The unit under test.
   QDomDocument dom_; ///< The dom document used for testing.
};

BOOST_FIXTURE_TEST_SUITE(ConstructStylings, ConstructStylingsFixture)

BOOST_AUTO_TEST_CASE(EmptyDom)
{
   Run();
   BOOST_CHECK_EQUAL(UUT_.styleTypes_.size(), 0);
   BOOST_CHECK_EQUAL(UUT_.pageSize_.isEmpty(), true);
}

BOOST_AUTO_TEST_SUITE(PageSize)

BOOST_AUTO_TEST_CASE(ZeroDpi)
{
   Setup("OsfFiles/ConstructStylings.osf", 0, 0);
   Run();
   BOOST_CHECK_EQUAL(UUT_.pageSize_.width(), 0);
   BOOST_CHECK_EQUAL(UUT_.pageSize_.height(), 0);
}

BOOST_AUTO_TEST_CASE(NonZeroDpi)
{
   Setup("OsfFiles/ConstructStylings.osf", 254, 254);
   Run();
   BOOST_CHECK_EQUAL(UUT_.pageSize_.width(), 85);
   BOOST_CHECK_EQUAL(UUT_.pageSize_.height(), 110);
}

BOOST_AUTO_TEST_SUITE_END() // PageSize

BOOST_AUTO_TEST_SUITE(Styles)

BOOST_AUTO_TEST_CASE(AllMissing)
{
   Setup("OsfFiles/ConstructStylings.osf", 254, 254);
   Run();
   VerifyStyling("All Missing",
      12.0,
      QFont::Capitalization::MixedCase,
      false,
      false,
      false,
      0.0,
      0.0,
      15.0,
      10.0,
      Qt::AlignLeft);
}

BOOST_AUTO_TEST_CASE(AllEmpty)
{
   Setup("OsfFiles/ConstructStylings.osf", 254, 254);
   Run();
   VerifyStyling("All Empty",
      12.0,
      QFont::Capitalization::MixedCase,
      false,
      false,
      false,
      0.0,
      0.0,
      15.0,
      10.0,
      Qt::AlignLeft);
}

BOOST_AUTO_TEST_CASE(RightAlign)
{
   Setup("OsfFiles/ConstructStylings.osf", 254, 254);
   Run();
   VerifyStyling("Right Align",
      12.0,
      QFont::Capitalization::MixedCase,
      false,
      false,
      false,
      0.0,
      0.0,
      15.0,
      10.0,
      Qt::AlignRight);
}

BOOST_AUTO_TEST_CASE(CenterAlign)
{
   Setup("OsfFiles/ConstructStylings.osf", 254, 254);
   Run();
   VerifyStyling("Center Align",
      12.0,
      QFont::Capitalization::MixedCase,
      false,
      false,
      false,
      0.0,
      0.0,
      15.0,
      10.0,
      Qt::AlignCenter);
}

BOOST_AUTO_TEST_CASE(AllCustom)
{
   Setup("OsfFiles/ConstructStylings.osf", 254, 254);
   Run();
   VerifyStyling("All Custom",
      5.5,
      QFont::Capitalization::AllUppercase,
      true,
      true,
      true,
      (2 / 6.0) * 254,
      0.0,
      15.0 + 22.0,
      10.0 + 22.0,
      Qt::AlignLeft);
}

BOOST_AUTO_TEST_SUITE_END() // Styles

BOOST_AUTO_TEST_SUITE_END() // ConstructStylings

BOOST_AUTO_TEST_SUITE_END() // ScreenplayTests

}