#include <boost/test/unit_test.hpp>
#include <QFileInfo>
#include <QFile>
#include <set>

#include "Fixtures/ScreenplayFixture.h"

namespace FirstRead
{

BOOST_AUTO_TEST_SUITE(ScreenplayTests)

/// @brief Fixture for testing reading characters from a QDomDocument.
struct GetCharactersFixture
{
   /// @brief Constructor
   GetCharactersFixture()
      : UUT_()
      , dom_()
   {

   }

   /// @brief Setup dom to test.
   /// @param fileName The file to use for populating the dom.
   void Setup(QString fileName)
   {
      QFileInfo fileInfo(fileName);
      if (fileInfo.exists() && fileInfo.isFile())
      {
         QFile file(fileInfo.filePath());
         BOOST_REQUIRE(file.open(QIODevice::ReadOnly));
         dom_.setContent(&file);
         file.close();
      }
   }

   /// @brief Gets the characters from dom.
   void Run()
   {
      UUT_.GetCharacters(dom_);
   }

   /// @brief Verify the populated characters from the dom.
   /// @param expected The expected characters.
   void Verify(std::set<QString> expected)
   {
      BOOST_CHECK_EQUAL(static_cast<int>(UUT_.GetCharacters().size()), expected.size());

      for (auto& res : UUT_.GetCharacters())
      {
         BOOST_CHECK_EQUAL(expected.count(res), 1);
      }
   }

   ScreenplayFixture UUT_; ///< The unit under test.
   QDomDocument dom_; ///< The dom document used for testing.
};

BOOST_FIXTURE_TEST_SUITE(GetCharacters, GetCharactersFixture)

BOOST_AUTO_TEST_CASE(NoEntries)
{
   Run();
   BOOST_CHECK_EQUAL(UUT_.styleTypes_.size(), 0);
   BOOST_CHECK_EQUAL(UUT_.pageSize_.isEmpty(), true);
}

BOOST_AUTO_TEST_CASE(Entries)
{
   Setup("OsfFiles/GetCharacters.osf");
   Run();
   Verify({ 
      "First Read Narrator",
      "LESLIE",
      "ANN",
      "MARK",
      "TOM",
      "RON",
      "APRIL",
      "ANDY",
      "BEN",
      "CHRIS",
      "GARRY JERRY LARRY TERRY BARRY",
      "DONNA"});
}

BOOST_AUTO_TEST_SUITE_END() // GetCharacters

BOOST_AUTO_TEST_SUITE_END() // ScreenplayTests

}