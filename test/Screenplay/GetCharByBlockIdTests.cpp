#include <boost/test/unit_test.hpp>

#include "Fixtures/ScreenplayFixture.h"

namespace FirstRead
{

BOOST_AUTO_TEST_SUITE(ScreenplayTests)

/// @brief Fixture for testing getting a character name by block Id.
struct GetCharacterByBlockIdFixture
{
   /// @brief Constructor
   GetCharacterByBlockIdFixture()
      : UUT_()
      , result_()
   {

   }

   /// @brief Setup the block Ids of character names.
   /// @param input Character names at their assigned block position.
   void Setup(std::vector<QString> input)
   {
      UUT_.charByBlock_ = input;
   }

   /// @brief Gets the character name of a specific block.
   /// @param i The block Id to get the character name for.
   void Run(unsigned int i)
   {
      result_ = UUT_.GetCharacterByBlockId(i);
   }

   /// @brief Verifies the resulting character name to an extected name.
   /// @param expected The expected name of the result.
   void Verify(const QString& expected)
   {
      BOOST_CHECK_EQUAL(result_.toStdString(), expected.toStdString());
   }

   ScreenplayFixture UUT_; ///< The unit under test.
   QString result_; ///< Resulting character name post run call.
};

BOOST_FIXTURE_TEST_SUITE(GetCharacterByBlockId, GetCharacterByBlockIdFixture)

BOOST_AUTO_TEST_CASE(NoCharacters)
{
   Setup({});
   Run(0);
   Verify("");
}

BOOST_AUTO_TEST_CASE(FirstCharacter)
{
   Setup({"A", "B", "C"});
   Run(0);
   Verify("A");
}

BOOST_AUTO_TEST_CASE(MidCharacter)
{
   Setup({ "A", "B", "C" });
   Run(1);
   Verify("B");
}

BOOST_AUTO_TEST_CASE(LastCharacter)
{
   Setup({ "A", "B", "C" });
   Run(2);
   Verify("C");
}

BOOST_AUTO_TEST_CASE(BlockIdTooLarge)
{
   Setup({ "A", "B", "C" });
   Run(3);
   Verify("");
}

BOOST_AUTO_TEST_SUITE_END() // GetCharacterByBlockId

BOOST_AUTO_TEST_SUITE_END() // ScreenplayTests

}