#include <boost/test/unit_test.hpp>

#include "Fixtures/VoicesFixture.h"

namespace FirstRead
{

BOOST_AUTO_TEST_SUITE(VoicesTests)

BOOST_FIXTURE_TEST_SUITE(DeleteVoice, VoicesFixture)

BOOST_AUTO_TEST_SUITE(NotExists)

BOOST_AUTO_TEST_CASE(EmptyName)
{
   BOOST_CHECK(!UUT_.DeleteVoice(""));
}

BOOST_AUTO_TEST_CASE(NonEmptyName)
{
   BOOST_CHECK(!UUT_.DeleteVoice("Voice Name"));
}

BOOST_AUTO_TEST_SUITE_END() // NotExists

BOOST_AUTO_TEST_CASE(Exists)
{
   UUT_.CreateVoice("Voice Name", VoiceType());
   BOOST_CHECK(UUT_.DeleteVoice("Voice Name"));
   BOOST_CHECK_EQUAL(UUT_.GetVoices().size(), 0);
}

BOOST_AUTO_TEST_SUITE_END() // DeleteVoice

BOOST_AUTO_TEST_SUITE_END() // VoicesTests

}