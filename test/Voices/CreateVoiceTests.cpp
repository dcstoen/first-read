#include <boost/test/unit_test.hpp>

#include "Fixtures/VoicesFixture.h"

namespace FirstRead
{

BOOST_AUTO_TEST_SUITE(VoicesTests)

BOOST_FIXTURE_TEST_SUITE(CreateVoice, VoicesFixture)

BOOST_AUTO_TEST_SUITE(SingleName)

BOOST_AUTO_TEST_CASE(EmptyName)
{
   BOOST_CHECK(!UUT_.CreateVoice("", VoiceType()));
   BOOST_CHECK_EQUAL(UUT_.GetVoices().size(), 0);
}

BOOST_AUTO_TEST_CASE(NonEmptyName)
{
   BOOST_CHECK(UUT_.CreateVoice("Voice Name", VoiceType()));
   BOOST_CHECK_EQUAL(UUT_.GetVoices().size(), 1);
}

BOOST_AUTO_TEST_SUITE_END() // SingleName

BOOST_AUTO_TEST_SUITE(MultipleNames)

BOOST_AUTO_TEST_CASE(Duplicate)
{
   BOOST_CHECK(UUT_.CreateVoice("Voice Name", VoiceType()));
   BOOST_CHECK(!UUT_.CreateVoice("Voice Name", VoiceType()));
   BOOST_CHECK_EQUAL(UUT_.GetVoices().size(), 1u);
}

BOOST_AUTO_TEST_CASE(NonDuplicate)
{
   BOOST_CHECK(UUT_.CreateVoice("Voice Name", VoiceType()));
   BOOST_CHECK(UUT_.CreateVoice("Another Voice Name", VoiceType()));
   BOOST_CHECK_EQUAL(UUT_.GetVoices().size(), 2u);
}

BOOST_AUTO_TEST_SUITE_END() // MultipleNames

BOOST_AUTO_TEST_SUITE(VoiceProperties)

BOOST_AUTO_TEST_CASE(Default)
{
   VoiceType expected;
   BOOST_CHECK(UUT_.CreateVoice("Voice Name", expected));

   // Check voice properties
   const VoiceType* prop = UUT_.GetVoiceProperties("Voice Name");
   BOOST_REQUIRE(prop);
   VerifyVoiceType(expected, *prop);
}

BOOST_AUTO_TEST_CASE(NonDefault)
{
   VoiceType expected = GetNonDefaultVoice();
   BOOST_CHECK(UUT_.CreateVoice("Voice Name", expected));

   // Check voice properties
   const VoiceType* prop = UUT_.GetVoiceProperties("Voice Name");
   BOOST_REQUIRE(prop);
   VerifyVoiceType(expected, *prop);
}

BOOST_AUTO_TEST_CASE(Overwritten)
{
   VoiceType expected = GetNonDefaultVoice();
   BOOST_CHECK(UUT_.CreateVoice("Voice Name", expected));

   expected.volume_ = 100.0;
   expected.rate_ = 75.0;
   expected.pitch_ = 50.0;
   BOOST_CHECK(!UUT_.CreateVoice("Voice Name", expected));

   // Check voice properties
   const VoiceType* prop = UUT_.GetVoiceProperties("Voice Name");
   BOOST_REQUIRE(prop);
   VerifyVoiceType(expected, *prop);
}

BOOST_AUTO_TEST_SUITE_END() // VoiceProperties

BOOST_AUTO_TEST_SUITE_END() // CreateVoice

BOOST_AUTO_TEST_SUITE_END() // VoicesTests

}