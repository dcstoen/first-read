#include <boost/test/unit_test.hpp>

#include "Fixtures/VoicesFixture.h"

namespace FirstRead
{

BOOST_AUTO_TEST_SUITE(VoicesTests)

BOOST_FIXTURE_TEST_SUITE(GetVoiceProperties, VoicesFixture)

BOOST_AUTO_TEST_CASE(Invalid)
{
   BOOST_CHECK(!UUT_.GetVoiceProperties("invalid name"));
}

BOOST_AUTO_TEST_CASE(Valid)
{
   VoiceType expected = GetNonDefaultVoice();
   UUT_.CreateVoice("valid name", expected);
   const VoiceType* prop = UUT_.GetVoiceProperties("valid name");
   
   BOOST_REQUIRE(prop);
   VerifyVoiceType(expected, *prop);
}

BOOST_AUTO_TEST_SUITE_END() // GetVoiceProperties

BOOST_AUTO_TEST_SUITE_END() // VoicesTests

}