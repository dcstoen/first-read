#ifndef VOICE_FIXTURE_H
#define VOICE_FIXTURE_H

#include <boost/test/unit_test.hpp>
#include "Voices/Voices.h"

namespace FirstRead
{

/// @brief Simple fixture for testing the Voices class.
struct VoicesFixture
{
   /// @brief Create and return a non default voice type.
   /// @return Non default voice type.
   VoiceType GetNonDefaultVoice()
   {
      VoiceType ret;
      ret.volume_ = 85.0;
      ret.rate_ = 75.0;
      ret.pitch_ = 65.0;

      return ret;
   }

   /// @brief Verify two voice types are equal.
   /// @param expected Voice type to verify against.
   /// @param result Voice type to verify with.
   void VerifyVoiceType(const VoiceType& expected, const VoiceType& result)
   {
      BOOST_CHECK_EQUAL(expected.voice_.name().toStdString(), result.voice_.name().toStdString());
      BOOST_CHECK_EQUAL(expected.voice_.gender(), result.voice_.gender());
      BOOST_CHECK_EQUAL(expected.voice_.age(), result.voice_.age());
      BOOST_CHECK_EQUAL(expected.locale_.name().toStdString(), result.locale_.name().toStdString());
      BOOST_CHECK_EQUAL(expected.locale_.language(), result.locale_.language());
      BOOST_CHECK_EQUAL(expected.locale_.country(), result.locale_.country());
      BOOST_CHECK_EQUAL(expected.volume_, result.volume_);
      BOOST_CHECK_EQUAL(expected.rate_, result.rate_);
      BOOST_CHECK_EQUAL(expected.pitch_, result.pitch_);
   }

   Voices UUT_; ///< Unit under test.
};

}

#endif // VOICE_FIXTURE_H