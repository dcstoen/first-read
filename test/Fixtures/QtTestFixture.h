#ifndef QT_TEST_FIXTURE_H
#define QT_TEST_FIXTURE_H

#include <QApplication>

namespace FirstRead
{

/// @brief Fixture for testing with Qt dependencies.
struct QtTestFixture
{
   /// @brief Constructor
   QtTestFixture()
   {
      int arg = 0;
      QApplication a(arg, 0);
   }
};

}

#endif // QT_TEST_FIXTURE_H