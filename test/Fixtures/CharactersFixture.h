#ifndef CHARACTER_FIXTURE_H
#define CHARACTER_FIXTURE_H

#include "Characters/Characters.h"
#include "Stubs/ScreenplayStub.h"

#include <list>
#include <QString>

namespace FirstRead
{

/// @brief Simple test fixture for testing Characters class.
struct CharactersFixture
{
   /// @brief Constructor
   CharactersFixture()
      : UUT_()
      , screenplay_()
   {

   }

   /// @brief Sets up the test environment by initializing the screenplay characters
   /// and loading the unit under test.
   /// @param setupChars Characters to initialize.
   void Setup(std::list<QString> setupChars)
   {
      screenplay_.AddCharacters(setupChars);
      UUT_.Load(screenplay_);
   }

   Characters UUT_; ///< Unit under test.
   ScreenplayStub screenplay_; ///< Screenplay stub for testing.
};

}

#endif //CHARACTER_FIXTURE_H