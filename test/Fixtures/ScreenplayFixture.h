#ifndef SCREENPLAY_FIXTURE_H
#define SCREENPLAY_FIXTURE_H

#include "Screenplay/Screenplay.h"

namespace FirstRead
{

/// @brief Simple fixture for testing the Screenplay class.
struct ScreenplayFixture : public Screenplay
{
public:
   /// @brief Constructor
   /// @param dpiX Logical pixels in the X direction that measures to one inch.
   /// @param dpiY Logical pixels in the Y direction that measures to one inch.
   ScreenplayFixture(int dpiX = 0, int dpiY = 0)
      : Screenplay(dpiX, dpiY)
   {

   }

   /// @name Exposed Members
   /// @{
   using Screenplay::ConstructStylings;
   using Screenplay::GetCharacters;
   using Screenplay::charByBlock_;
   using Screenplay::styleTypes_;
   using Screenplay::pageSize_;
   using Screenplay::dpiX_;
   using Screenplay::dpiY_;
   /// @}
};

}

#endif // SCREENPLAY_FIXTURE_H