#include <boost/test/unit_test.hpp>
#include <set>
#include <QString>

#include "Fixtures/CharactersFixture.h"
#include "Stubs/ScreenplayStub.h"

namespace FirstRead
{

BOOST_AUTO_TEST_SUITE(CharactersTests)

/// @brief Simple test fixture for testing loading characters from a screenplay.
struct LoadFromScreenplayFixture : public CharactersFixture
{
   /// @brief Verifies all expected characters are contained in the character list and no more.
   /// @param expected The expected characters in the list.
   void Verify(std::set<QString> expected)
   {
      BOOST_CHECK_EQUAL(static_cast<int>(UUT_.GetCharacters().size()), expected.size());
   
      for (auto& res : UUT_.GetCharacters())
      {
         BOOST_CHECK_EQUAL(expected.count(res), 1);
      }
   }
};

BOOST_FIXTURE_TEST_SUITE(LoadFromScreenplay, LoadFromScreenplayFixture)

BOOST_AUTO_TEST_CASE(NullName)
{
   Setup({});
   Verify({});
}

BOOST_AUTO_TEST_CASE(EmptyName)
{
   Setup({ "" });
   Verify({});
}

BOOST_AUTO_TEST_CASE(NonEmptyNames)
{
   Setup({ "JEAN VALJEAN", "JAVERT", "24601" });
   Verify({ "JEAN VALJEAN", "JAVERT", "24601" });
}

BOOST_AUTO_TEST_SUITE_END() // LoadFromScreenplay

BOOST_AUTO_TEST_SUITE_END() // CharactersTests

}