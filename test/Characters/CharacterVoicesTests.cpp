#include <boost/test/unit_test.hpp>
#include <set>

#include "Characters/Characters.h"
#include "Fixtures/CharactersFixture.h"

namespace FirstRead
{

BOOST_AUTO_TEST_SUITE(CharactersTests)

BOOST_FIXTURE_TEST_SUITE(CharacterVoices, CharactersFixture)

BOOST_AUTO_TEST_CASE(MissingVoice)
{
   Setup({ "A" });
   BOOST_CHECK_EQUAL(UUT_.GetCharacterVoice("A").toStdString(), "");
}

BOOST_AUTO_TEST_CASE(MissingCharacter)
{
   Setup({});
   BOOST_CHECK_EQUAL(UUT_.GetCharacterVoice("A").toStdString(), "");
}

BOOST_AUTO_TEST_CASE(WithVoice)
{
   Setup({"A"});
   UUT_.SetCharacterVoice("A", "voiceA");
   BOOST_CHECK_EQUAL(UUT_.GetCharacterVoice("A").toStdString(), "voiceA");
}

BOOST_AUTO_TEST_CASE(WithOverwrittenVoice)
{
   Setup({ "A" });
   UUT_.SetCharacterVoice("A", "voiceA");
   UUT_.SetCharacterVoice("A", "OverwrittenVoiceA");
   BOOST_CHECK_EQUAL(UUT_.GetCharacterVoice("A").toStdString(), "OverwrittenVoiceA");
}

BOOST_AUTO_TEST_SUITE_END() // CharacterVoices

BOOST_AUTO_TEST_SUITE_END() // CharactersTests

}