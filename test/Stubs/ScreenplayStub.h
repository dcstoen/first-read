#ifndef SCREENPLAY_STUB_H
#define SCREENPLAY_STUB_H

#include <QString>
#include "Screenplay/Screenplay.h"

namespace FirstRead
{

/// @brief Stub screenplay for testing.
class ScreenplayStub : public Screenplay
{
public:
   /// @brief Constructor.
   ScreenplayStub(int dpiX = 0, int dpiY = 0)
      : Screenplay(dpiX, dpiY)
   {

   }

   /// @brief Adds the given characters to the character list.
   /// @param charToAdd The character names to add.
   void AddCharacters(std::list<QString> charToAdd)
   {
      for (auto& c : charToAdd)
      {
         characters_.push_back(c);
      }
   }
};

}

#endif // SCREENPLAY_STUB_H