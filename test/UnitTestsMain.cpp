// The following results in a single main function that can run any tests from and number
// of other sources compiled into the executable.
#define BOOST_TEST_MODULE "FirstReadTests"
#define BOOST_TEST_STATIC_LINK

#include <boost/test/unit_test.hpp>
#include "Fixtures/QtTestFixture.h"

namespace FirstRead
{
BOOST_TEST_GLOBAL_FIXTURE(QtTestFixture);
}