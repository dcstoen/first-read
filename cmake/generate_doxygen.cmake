find_package(Doxygen OPTIONAL_COMPONENTS dot)

function(generate_doxygen)
   if (DOXYGEN_FOUND)
      # Set Doxyfile parameters where DOXYGEN_<Name> relates to <Name> in the Doxyfile.
      set(DOXYGEN_PROJECT_NAME ${PRODUCT_NAME})
      set(DOXYGEN_PROJECT_NUMBER ${PROJECT_VERSION})
      set(DOXYGEN_PROJECT_BRIEF ${PROJECT_DESCRIPTION})
      set(DOXYGEN_PROJECT_LOGO "res/icons/icon.png")
      set(DOXYGEN_OUTPUT_DIRECTORY "doc/")
      set(DOXYGEN_INPUT 
         README.md 
         LICENSE.md 
         ./src)
      set(DOXYGEN_IMAGE_PATH "doc/imgs/")
      set(DOXYGEN_HTML_EXTRA_FILES "doc/FirstReadSampleScript.osf")
      set(DOXYGEN_USE_MDFILE_AS_MAINPAGE "README.md")
      set(DOXYGEN_GENERATE_HTML YES)
      set(DOXYGEN_GENERATE_TREEVIEW YES)
      set(DOXYGEN_VERBATIM_HEADERS NO)
      
      doxygen_add_docs(
         Doxygen
         WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
         COMMENT "Generating doxygen..."
      )
   endif (DOXYGEN_FOUND)
endfunction(generate_doxygen)