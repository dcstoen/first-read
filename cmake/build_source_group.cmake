cmake_minimum_required(VERSION 3.4)

# Define a function that both adds sources to the given target and 
# organizes them into a given group.
function(build_source_group targetName groupName)
   foreach(item IN ITEMS ${ARGN})
      get_filename_component(sourceDir ${CMAKE_CURRENT_LIST_DIR}/${item} DIRECTORY)
      list(APPEND sources ${CMAKE_CURRENT_LIST_DIR}/${item})
   endforeach()

   target_sources(${targetName} PRIVATE ${sources})
   source_group(${groupName} FILES ${sources})
endfunction(build_source_group)