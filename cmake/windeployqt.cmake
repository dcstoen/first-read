find_package(Qt5 COMPONENTS Core REQUIRED)

# get absolute path to qmake, then use it to find windeployqt executable

get_target_property(_qmake_executable Qt5::qmake IMPORTED_LOCATION)
get_filename_component(_qt_bin_dir "${_qmake_executable}" DIRECTORY)

function(build_with_windeployqt target)
   add_custom_command(TARGET ${target} POST_BUILD
      COMMAND "${_qt_bin_dir}/windeployqt.exe"
              \"$<TARGET_FILE:${target}>\"
              --no-angle
              --no-opengl
              --no-opengl-sw
              --no-compiler-runtime
              --no-system-d3d-compiler
      COMMENT "Deploying Qt libraries for '${target}' ..."
   )
endfunction(build_with_windeployqt)

function(install_with_windeployqt installDir projectName)
   install(CODE 
      "execute_process(COMMAND \"${_qt_bin_dir}/windeployqt.exe\"
         \"${installDir}/${projectName}.exe\"
         --no-angle
         --no-opengl
         --no-opengl-sw
         --no-compiler-runtime
         --no-system-d3d-compiler)"
   )
endfunction(install_with_windeployqt)