#include "CharacterBank.h"
#include "ui_CharacterBank.h"
#include "Voices/Voices.h"

#include <QComboBox>
#include <QTableWidget>

namespace FirstRead
{

CharacterBank::CharacterBank(QWidget *parent)
   : QWidget(parent)
   , ui(new Ui::CharacterBankDock)
   , currentVoices_()
   , characters_()
{
   ui->setupUi(this);
}

CharacterBank::~CharacterBank()
{
   delete ui;
}

void CharacterBank::LoadCharacters(const Screenplay& screenplay)
{
   characters_.Load(screenplay);
   
   // Clear anything previously loaded.
   ui->characterTable->setRowCount(0);
   // Populate the character bank.
   ui->characterTable->setRowCount(static_cast<int>(characters_.GetCharacters().size()));
   auto charIt = characters_.GetCharacters().begin();
   int i = 0;
   for (; charIt != characters_.GetCharacters().end(); ++charIt, ++i)
   {
      ui->characterTable->setItem(i, 0, new QTableWidgetItem(*charIt));
      ui->characterTable->setRowHeight(i, 30);

      QComboBox* availableVoices = new QComboBox(ui->characterTable);
      availableVoices->setProperty("row", i);
      
      // Populate all character's voice selection comboboxes.
      PopulateAvailableVoices(availableVoices);
      
      ui->characterTable->setCellWidget(i, 1, availableVoices);
      QObject::connect(availableVoices, SIGNAL(currentIndexChanged(int)), this, SLOT(VoiceSelectionChanged(int)));
      availableVoices->currentIndexChanged(0);
   }
}

void CharacterBank::ExportCharacterVoices(QDataStream& os)
{
   os << static_cast<quint32>(characters_.GetCharacters().size());
   for (auto& character : characters_.GetCharacters())
   {
      os << character
         << characters_.GetCharacterVoice(character);
   }
}

void CharacterBank::ImportCharacterVoices(QDataStream& is)
{
   quint32 size;
   QString charName;
   QString charVoice;
   is >> size;
   for (quint32 i = 0; i < size; ++i)
   {
      is >> charName
         >> charVoice;

      SetVoiceSelection(charName, charVoice);
   }
}

QString CharacterBank::GetCharacterVoice(const QString& character)
{
   return characters_.GetCharacterVoice(character);
}

void CharacterBank::AddVoice(QString name)
{
   currentVoices_.push_back(name);
   for (int i = 0; i < ui->characterTable->rowCount(); ++i)
   {
      static_cast<QComboBox*>(ui->characterTable->cellWidget(i, 1))->addItem(name);
   }
}

void CharacterBank::RemoveVoice(QString name)
{
   currentVoices_.removeAll(name);
   for (int i = 0; i < ui->characterTable->rowCount(); ++i)
   {
      QString currName = ui->characterTable->item(i, 0)->text();
      QComboBox* widget = static_cast<QComboBox*>(ui->characterTable->cellWidget(i, 1));
      widget->removeItem(widget->findText(name));
      if (currName == name)
      {
         widget->setCurrentIndex(-1);
      }
   }
}

void CharacterBank::PopulateAvailableVoices(QComboBox* availableVoices)
{
   for (auto& voice : currentVoices_)
   {
      availableVoices->addItem(QString::fromStdString(voice.toStdString()));
   }
}

void CharacterBank::VoiceSelectionChanged(int i)
{
   int row = sender()->property("row").toInt();
   QString charName = ui->characterTable->item(row, 0)->text();
   QComboBox* widget = static_cast<QComboBox*>(ui->characterTable->cellWidget(row, 1));
   QString voiceName = widget->itemText(i);
   if (!voiceName.isEmpty())
   {
      characters_.SetCharacterVoice(charName, widget->itemText(i));
   }
}

void CharacterBank::SetVoiceSelection(const QString& character, const QString& voice)
{
   for (int i = 0; i < ui->characterTable->rowCount(); ++i)
   {
      if (character == ui->characterTable->item(i, 0)->text())
      {
         QComboBox* widget = static_cast<QComboBox*>(ui->characterTable->cellWidget(i, 1));
         int index = widget->findText(voice);
         widget->setCurrentIndex(index);
      }
   }
}

}