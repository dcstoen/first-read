#include "Characters.h"

namespace FirstRead
{

Characters::Characters()
   : charVoiceDB_()
   , chars_()
{

}

Characters::~Characters()
{

}

void Characters::Load(const Screenplay& screenplay)
{
   charVoiceDB_.clear();
   chars_.clear();

   for (auto& character : screenplay.GetCharacters())
   {
      if (!character.isEmpty())
      {
         if (!charVoiceDB_.count(character))
         {
            charVoiceDB_.insert({ character, "" });
            chars_.push_back(character);
         }
      }
   }
}

void Characters::SetCharacterVoice(const QString& character, const QString& voice)
{
   charVoiceDB_[character] = voice;
}

QString Characters::GetCharacterVoice(const QString& character)
{
   auto it = charVoiceDB_.find(character);
   return (it != charVoiceDB_.end()) ? it->second : "";
}

}