#ifndef CHARACTERS_H
#define CHARACTERS_H

#include <list>
#include <map>
#include <string>
#include "Screenplay/Screenplay.h"

namespace FirstRead
{

/// @brief Character database with their corresponding voices.
class Characters
{
public:
   /// @brief Default constructor.
   Characters();

   /// @brief Destructor.
   virtual ~Characters();

   /// @brief Populate the database with the characters from the screenplay.
   /// @param screenplay The screenplay to use when loading the database.
   void Load(const Screenplay& screenplay);

   /// @brief Get all character names in the databaes.
   /// @return A list of character names.
   const QStringList& GetCharacters();

   /// @brief Fetches the voice of a given character.
   /// @param character The character to fetch the voice for.
   /// @return The name of the voice assigned to the given character.
   QString GetCharacterVoice(const QString& character);
   
   /// @brief Assigns a voice to a given character.
   /// @param character Name of the character to assign a voice to.
   /// @param voice Name of the voice to assign to the character.
   void SetCharacterVoice(const QString& character, const QString& voice);

protected:
   std::map<QString, QString> charVoiceDB_; ///< Map of character names to assigned voices.
   QStringList chars_; ///< List of all characters in the database.
};

inline const QStringList& Characters::GetCharacters()
{
   return chars_;
}

}

#endif // CHARACTERS_H
