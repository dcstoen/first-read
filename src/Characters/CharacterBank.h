#ifndef CHARACTER_BANK_H
#define CHARACTER_BANK_H

#include <qwidget.h>
#include "Characters.h"
#include "Screenplay/Screenplay.h"

// Forward declarations.
class Voices;
class QComboBox;

namespace Ui
{
   class CharacterBankDock;
}

namespace FirstRead
{

/// @brief UI logic for character voice selections.
class CharacterBank : public QWidget
{
   Q_OBJECT

public:
   /// @brief Constructor
   /// @param parent Pointer to the owner of this QWidget.
   explicit CharacterBank(QWidget *parent = 0);

   /// @brief Destructor
   virtual ~CharacterBank();

   /// @brief Load characters to the character bank.
   /// @param screenplay Screenplay to load the characters from.
   /// @note Overrides all existing characters.
   void LoadCharacters(const Screenplay& screenplay);

   /// @brief Fetches the name of the voice assigned to a given character.
   /// @param character The character to fetch the voice of.
   /// @return The voice assigned to the character.
   QString GetCharacterVoice(const QString& character);

   /// @brief Serializes the current saved character assignments.
   /// @param os Output serialized data stream.
   void ExportCharacterVoices(QDataStream& os);

   /// @brief Generates saved character assignments from serializes data.
   /// @param is Input serialized data stream.
   void ImportCharacterVoices(QDataStream& is);

public slots:
   /// @brief Slot for when a voice is added to the voice database.
   /// @param name Name of the voice being added.
   void AddVoice(QString name);

   /// @brief Slot for when a voice is removed from the voice database.
   /// @param name Name of the voice being removed.
   void RemoveVoice(QString name);

   /// @brief Slot for when a character's voice selection changes.
   /// @param i Index of the voice selection combobox. 
   void VoiceSelectionChanged(int i);

private:
   /// @brief Populates a character's voice selection with the current available voices.
   /// @param availableVoices The combobox to a character's voice selection.
   void PopulateAvailableVoices(QComboBox* availableVoices);

   /// @brief Sets the combo box selection for a given character to a specified voice.
   /// @param character The character of the combo box.
   /// @param voice The voice to select in the combo box.
   void SetVoiceSelection(const QString& character, const QString& voice);

   Ui::CharacterBankDock* ui; ///< UI
   QStringList currentVoices_; ///< List of the current voices available.
   Characters characters_; ///< Logic class for the character-voice maintance.
};

}

#endif // CHARACTER_BANK_H
