#ifndef OSF_PARSER_BASE_H
#define OSF_PARSER_BASE_H

#include "FileParsers/FileParserBase.h"

namespace FirstRead
{

/// @brief Parser for Open Screenplay Format files.
class OsfParser : public FileParserBase
{
public:
   /// @brief Default constructor.
   OsfParser();

   /// @brief Destructor.
   virtual ~OsfParser();

   /// @brief Parse a file into a screenplay.
   /// @param fileInfo The file to parse.
   /// @param parsedScreenplay The resulting screenplay.
   /// @return True if the parsing was successful.
   virtual bool ParserFile(const QFileInfo &fileInfo, Screenplay& parsedScreenplay) const;

protected:
   /// @brief Standardizes attribute names to a common convention.
   /// @param doc The document to change attributes in.
   void StandardizeAttrs(QString& doc) const;

   /// @brief Map of alltributes to rename.
   std::map<QString, QString> attrRenames_;

private:
   // Prevent default copy contructor and assignment operator
   OsfParser(const OsfParser& copy);
   const OsfParser& operator=(const OsfParser& rhs);
};

}

#endif // OSF_PARSER_BASE_H
