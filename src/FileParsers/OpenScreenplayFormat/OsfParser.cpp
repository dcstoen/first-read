#include "OsfParser.h"
#include <QDomDocument>
#include <QTextStream>

namespace FirstRead
{

OsfParser::OsfParser()
{
   // Populate attribute renames as {oldAttr, newAttr}.
   attrRenames_ = 
   {
      {"page_width", "pageWidth"},
      {"page_height", "pageHeight"},
      {"margin_top", "marginTop"},
      {"margin_bottom", "marginBottom"},
      {"margin_left", "marginLeft"},
      {"margin_right", "marginRight"},
      {"normal_linesperinch", "normalLinesPerInch"},
      {"element_spacing", "elementSpacing"},
      {"break_on_sentences", "breakOnSentences"},
      {"cont_text", "contText"},
      {"more_text", "moreText"},
      {"scenes_continue", "scenesContinue"},
      {"continued_text", "continuedText"},
      {"scene_time_separator", "sceneTimeSeparator"},
      {"builtin_index", "builtInIndex"},
      {"basestylename", "baseStyleName"},
      {"style_enter", "styleEnter"},
      {"style_tab_before", "styleTabBefore"},
      {"style_tab_after", "styleTabAfter"},
      {"spacebefore", "spaceBefore"},
      {"keepwithnext", "keepWithNext"},
      {"allcaps", "allCaps"},
      {"leftindent", "leftIndent"},
      {"rightindent", "rightIndent"},
      {"color_name", "colorName"},
      {"color_index", "colorIndex"}
   };
}

OsfParser::~OsfParser()
{

}

bool OsfParser::ParserFile(const QFileInfo &fileInfo, Screenplay& parsedScreenplay) const
{
   bool success = false;
   QFile file(fileInfo.filePath());

   if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
       return false;

   QTextStream stream(&file);
   QString s = stream.readAll();
   StandardizeAttrs(s);
   
   QDomDocument doc;
   success = doc.setContent(s);

   parsedScreenplay.Load(doc);

   file.close();
   return success;
}

void OsfParser::StandardizeAttrs(QString& doc) const
{
   for (auto& it : attrRenames_)
   {
      doc.replace(it.first + "=\"", it.second + "=\"");
   }
}

}