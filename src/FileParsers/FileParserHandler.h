#ifndef FILE_PARSER_HANDLER_H
#define FILE_PARSER_HANDLER_H

#include <QFileInfo>
#include <list>
#include <string>
#include <unordered_map>
#include "Screenplay/Screenplay.h"

namespace FirstRead
{

// Forward declaration.
class FileParserBase;

/// @brief Class that controls what file parser to call based on file type.
/// Currently supporting file types: 
/// + *.osf 
/// + *.fadein 
/// + *.xml
/// @note All XML files are assumed to be in the OSF format.
class FileParserHandler
{
public:
   /// @brief Default constructor
   FileParserHandler();

   /// @brief Destructor
   ~FileParserHandler();

   /// @brief Parser a given file.
   /// @param fileName The file path and name of the file to parser.
   /// @param parsedScreenplay The parsed data.
   /// @retval true If the file was successfully parsed.
   bool ParserFile(const std::string& fileName, Screenplay& parsedScreenplay);

   /// @brief Get all supporting file types which can be parsed.
   /// @return Vector of all supporting file types.
   const std::vector<std::string> GetSupportedFileTypes() const;

protected:
   /// @brief Get the file parser for a given file extension.
   /// @param fileInfo The file to fetch the parser for.
   /// @return A pointer to the file parser. If no parser is found nullptr is returned.
   FileParserBase const* GetFileParser(const QFileInfo &fileInfo) const;

   /// @brief Lookup to the correct file parser for a given file extension.
   std::unordered_map<std::string, FileParserBase*> fileParserLookup_;

private:
   // Prevent default copy contructor and assignment operator
   FileParserHandler(const FileParserHandler& copy);
   const FileParserHandler& operator=(const FileParserHandler& rhs);
};

}

#endif // FILE_PARSER_HANDLER_H
