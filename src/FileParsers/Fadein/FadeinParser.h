#ifndef FADEIN_PARSER_BASE_H
#define FADEIN_PARSER_BASE_H

#include "FileParsers/FileParserBase.h"

namespace FirstRead
{

/// @brief Parser for Fadein files.
class FadeinParser : public FileParserBase
{
public:
   /// @brief Default constructor.
   FadeinParser();

   /// @brief Destructor.
   virtual ~FadeinParser();

   /// @brief Parse a file into a screenplay.
   /// @param fileInfo The file to parse.
   /// @param parsedScreenplay The resulting screenplay.
   /// @return True if the parsing was successful.
   virtual bool ParserFile(const QFileInfo& fileInfo, Screenplay& parsedScreenplay) const;

private:
   // Prevent default copy contructor and assignment operator
   FadeinParser(const FadeinParser& copy);
   const FadeinParser& operator=(const FadeinParser& rhs);
};

}

#endif // FADEIN_PARSER_BASE_H
