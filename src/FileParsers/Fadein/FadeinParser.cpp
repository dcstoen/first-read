#include "FadeinParser.h"
#include <QDomDocument>

namespace FirstRead
{

FadeinParser::FadeinParser()
{

}

FadeinParser::~FadeinParser()
{

}

bool FadeinParser::ParserFile(const QFileInfo &fileInfo, Screenplay& parsedScreenplay) const
{
   bool success = false;
   QFile file(fileInfo.filePath());

   if (!file.open(QIODevice::ReadOnly))
       return false;

   QByteArray compressedFadeinFile = file.readAll();

   QByteArray uncompressedFadeinFile = qUncompress(compressedFadeinFile);

   QDomDocument doc;
   success = doc.setContent(&file);

   parsedScreenplay.Load(doc);

   file.close();
   return success;
}

}