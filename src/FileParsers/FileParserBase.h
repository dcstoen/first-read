#ifndef FILE_PARSER_BASE_H
#define FILE_PARSER_BASE_H

#include <QFileInfo>
#include "Screenplay/Screenplay.h"

namespace FirstRead
{

/// @brief A base class for all file parsers.
class FileParserBase
{
public:
   /// @brief  Default constructor.
   FileParserBase();

   /// @brief Destructor.
   virtual ~FileParserBase();

   /// @brief Parse a file into a screenplay.
   /// @param fileInfo The file to parse.
   /// @param o_parsedScreenplay The resulting screenplay.
   /// @return True if the parsing was successful.
   virtual bool ParserFile(const QFileInfo& fileInfo, Screenplay& o_parsedScreenplay) const = 0;

private:
   // Prevent default copy contructor and assignment operator.
   FileParserBase(const FileParserBase& copy);
   const FileParserBase& operator=(const FileParserBase& rhs);
};

}

#endif // FILE_PARSER_BASE_H
