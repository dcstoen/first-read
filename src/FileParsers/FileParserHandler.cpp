#include "FileParserHandler.h"
#include "FileParserBase.h"
#include "OpenScreenplayFormat/OsfParser.h"

namespace FirstRead
{

FileParserHandler::FileParserHandler()
   : fileParserLookup_()
{
   fileParserLookup_.insert({"fadein", new OsfParser()}); /// @todo create fadein parser
   fileParserLookup_.insert({"osf", new OsfParser()});
   fileParserLookup_.insert({"xml", new OsfParser()}); // Assume xml files are in the OSF format
}

FileParserHandler::~FileParserHandler()
{
   auto it = fileParserLookup_.begin();
   for (; it != fileParserLookup_.end(); ++it)
   {
      delete (*it).second;
      (*it).second = 0;
   }
}

const std::vector<std::string> FileParserHandler::GetSupportedFileTypes() const
{
   std::vector<std::string> fileExtensions;
   fileExtensions.reserve(fileParserLookup_.size());

   for (auto& it : fileParserLookup_)
   {
      fileExtensions.push_back(it.first);
   }

   return fileExtensions;
}

bool FileParserHandler::ParserFile(const std::string& fileName, Screenplay& parsedScreenplay)
{
   bool successful = false;

   QFileInfo fileInfo(QString::fromStdString(fileName));
   FileParserBase const* parserPtr = GetFileParser(fileInfo);
   if (parserPtr != nullptr)
   {
      successful = parserPtr->ParserFile(fileInfo, parsedScreenplay);
   }

   return successful;
}

FileParserBase const* FileParserHandler::GetFileParser(const QFileInfo &fileInfo) const
{
   // Only return a valid file parser if it is a supported type
   auto it = fileParserLookup_.find(fileInfo.completeSuffix().toStdString());
   return (it != fileParserLookup_.end()) ? it->second : nullptr;
}

}