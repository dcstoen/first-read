#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextToSpeech>
#include <QGraphicsScene>
#include <QMenu>
#include "FileParsers/FileParserHandler.h"
#include "Screenplay/PageGraphicsView.h"
#include "Screenplay/PageGraphicsTextItem.h"
#include "Screenplay/Screenplay.h"
#include "Characters/Characters.h"
#include "Characters/CharacterBank.h"
#include "Voices/VoiceEditor.h"

namespace Ui
{
   class FirstReadUi;
}

namespace FirstRead
{

/// @brief UI for the main window.
class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   /// @brief Constructor.
   /// @param parent Pointer to the owner of this QWidget.
   explicit MainWindow(QWidget *parent = 0);
   
   /// @brief Destructor.
   ~MainWindow();

public slots:
   /// @name File Menu Controls
   /// @{
   void OpenFileClicked();
   void ImportVoicesClicked();
   void ExportVoicesClicked();
   void EngineChanged(QAction* action);
   void DarkModeClicked(bool clicked);
   /// @}

   /// @name Toolbar Controls
   /// @{
   void ZoomIn();
   void FitToView();
   void ZoomOut();
   void ToBeginning();
   void SkipPrevious();
   void PlayPause();
   void SkipNext();
   void ToEnd();
   void Stop();
   void Mute();
   /// @}

   /// @brief Slot for when the active speech class changes states.
   /// @param state The changed state of the speech class. 
   void SpeechStateChange(QTextToSpeech::State state);

   /// @name About Displays
   /// @{
   void AboutFirstReadClicked();
   void AboutQtClicked();
   /// @}

   /// @brief Slot for viewing the program help documents.
   void HelpClicked();

protected:
   /// @brief Enumerations for the state of the speach engine.
   enum class PlayStates
   {
      STOPPED_ = 0,
      PLAYING_,
      SEEKING_,
      PAUSED_,
   };

   /// @name Constructor Helpers
   /// @{
   void SetupSupportedFileTypes();
   void SetupMenu();
   void SetupToolbar();
   void SetupDocks();
   /// @}

   /// @brief Reads the next block of text in the screenplay.
   void ReadBlock(int block);

   /// @brief Refresh the UI display.
   void RefreshDisplay();

   /// @brief Stops the speech engine and resets the current block.
   void Reset();

   /// @brief Enables or disables the toolbar zoom buttons.
   void SetZoomButtonsEnabled();

   FileParserHandler fileParse_; ///< Holds the available file parsers.
   Screenplay screenplay_; ///< Currently loaded screenplay.
   QTextDocument screenplayDoc_; ///< The screenplay document object.
   QString fileTypesFilter_; ///< String for the filter used in the "Open File" screen.
   QString openedFile_; ///< Currently opened screenplay file.
   QString engineName_; ///< Current engine selected.
   PlayStates playState_; ///< The current state of the speak engine.
   double activeVolume_; ///< The volume fo the current text block being read.
   int currentTextBlock_; ///< Index of the currently active text block being read.
   int currentZoomScale_; ///< The current scale of the zoom.
   bool mute_; ///< Flag to indicate if the speak engine is mutted.

   const static double ZOOM_IN_AMOUNT_; ///< The amount of zoom on a single click.
   const static int ZOOM_CEIL_; ///< The maximum amount of zoom possible.
   const static int ZOOM_FLOOR_; ///< The minimum amount of zoom possible.

private:
   /// @name UI Elements
   /// @{
   Ui::FirstReadUi* ui;
   QTextToSpeech* speech_;
   VoiceEditor* voiceEditorUi_;
   CharacterBank* characterBankUi_;
   PageGraphicsView* pageView_;
   PageGraphicsTextItem* page_;
   QGraphicsScene* scene_;
   QActionGroup* engineGroup_;
   QMenu* engineMenu_;
   QAction* zoomOutAction_;
   QAction* fitToViewAction_;
   QAction* zoomInAction_;
   QAction* jumpBeginningAction_;
   QAction* jumpPrevAction_;
   QAction* playAction_;
   QAction* jumpNextAction_;
   QAction* jumpEndAction_;
   QAction* stopAction_;
   QAction* muteAction_;
   /// @}
};

}

#endif // MAINWINDOW_H
