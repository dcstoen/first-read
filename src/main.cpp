#include "mainwindow.h"
#include <QApplication>
#include <QStyleFactory>

int main(int argc, char *argv[])
{
   QApplication a(argc, argv);
   a.setWindowIcon(QIcon(":/icons/icon.svg"));
   
   a.setStyle(QStyleFactory::create("Fusion"));
   QFile file(":/Silverlight.qss");
   file.open(QFile::ReadOnly | QFile::Text);
   a.setStyleSheet(file.readAll());

   FirstRead::MainWindow w;
   w.show();
   return a.exec();
}
