#ifndef VERSION_UTILS_H
#define VERSION_UTILS_H

#include <QString>
#include <QStringList>
#include <QDataStream>
#include "version.h"

namespace FirstRead
{

/// @brief Helpful functions for dealing with the First Read version number.
namespace VersionUtils
{

/// @brief Simple structure of a parsed version number.
struct VersionType
{
   /// @brief Constructor
   VersionType()
      : major_(0)
      , minor_(0)
      , patch_(0)
      , tweak_(0)
   {

   }

   unsigned int major_; ///< The major number.
   unsigned int minor_; ///< The minor number.
   unsigned int patch_; ///< The patch number.
   unsigned int tweak_; ///< The tweak number.
};

/// @brief Gets the program version number.
/// @param ret The parsed version number.
void GetVersionNumber(VersionType& ret)
{
   QString ver(VER_PRODUCTVERSION_STR);
   QStringList sep = ver.split('.');
   
   ret.major_ = sep.at(0).toUInt();
   ret.minor_ = sep.at(1).toUInt();
   ret.patch_ = sep.at(2).toUInt();
   ret.tweak_ = sep.at(3).toUInt();
}

/// @brief Gets a string of the version number.
/// @param v The version number to get a string of.
/// @return The string of the version number.
QString GetVersionString(const VersionType& v)
{
   return QString("%1.%2.%3.%4").arg(v.major_).arg(v.minor_).arg(v.patch_).arg(v.tweak_);
}

/// @brief Determines if a given voice import file version is compatible 
/// with this version of First Read.
/// @param voiceVersion The parsed version number of the voice import file.
/// @return True if the provided voice import file version number is compatible.
bool CompatibleVoiceImportFile(const VersionType& voiceVersion)
{
   // No current limitations.
   return true;
}

/// @brief Stream a version number to an output stream.
/// @param voiceVersion The version number to stream.
/// @param os The output stream.
void StreamVersionNumber(const VersionType& voiceVersion, QDataStream& os)
{
   os << voiceVersion.major_
      << voiceVersion.minor_
      << voiceVersion.patch_
      << voiceVersion.tweak_;
}

/// @brief Get a version number from an input stream.
/// @param voiceVersion The version number in the stream.
/// @param is The input stream.
void GetVersionNumberFromStream(VersionType& voiceVersion, QDataStream& is)
{
   is >> voiceVersion.major_
      >> voiceVersion.minor_
      >> voiceVersion.patch_
      >> voiceVersion.tweak_;
}

}

}
#endif // VERSION_UTILS_H