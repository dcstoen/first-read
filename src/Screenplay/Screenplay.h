#ifndef SCREENPLAY_H
#define SCREENPLAY_H

#include <QDomDocument>
#include <QTextDocument>
#include <QTextFormat>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <string>

namespace FirstRead
{

// Forward declare
class Characters;
class Voices;

/// @brief Internal screenplay object.
class Screenplay
{
public:
   /// @brief Default constructor.
   /// @param dpiX Logical pixels in the X direction that measures to one inch.
   /// @param dpiY Logical pixels in the Y direction that measures to one inch.
   Screenplay(int dpiX, int dpiY);

   /// @brief Destructor.
   virtual ~Screenplay();

   /// @brief Initialize the screenplay from a converted QDomDocument 
   /// representing the OSF file format.
   /// @param doc The OSF style DOM document to initialize with. 
   void Load(const QDomDocument& doc);

   /// @brief Gets all characters in the screenplay.
   /// @return A list of character names.
   const QStringList& GetCharacters() const;

   /// @brief Gets a QTextDocument representing the screenplay.
   /// @return doc The text document to populate.
   void ToTextDocument(QTextDocument& doc);

   /// @brief Gets the character associated with a text block in the QTextDocument.
   /// @param blockId Index of the text document to get the associated character for.
   /// @return The name of the character associated with the given text block.
   QString GetCharacterByBlockId(unsigned int blockId) const;

   /// @brief Returns if a screenplay has been loaded.
   /// @return True if a screenplay has been loaded.
   bool IsLoaded();

protected:
   /// @name Internal Conversion Functions
   /// @{
   bool GetSettings(const QDomDocument& doc,
      int& height,
      int& width,
      int& topMargin,
      int& bottomMargin,
      int& leftMargin,
      int& rightMargin,
      double& linesPerInch);
   void ConstructStylings(const QDomDocument& doc);
   void GetCharacters(const QDomDocument& doc);
   void PopulateTextDocument(const QDomDocument& doc, QTextDocument& textDoc);
   /// @}

   /// Default character name associated with non-dialoge text blocks.
   const static QString DEFAULT_ELEMENT_OWNER_;
   /// Conversion from measurement units used in OSF (10^-4 meters) to inches.
   const static double OSF_TO_INCHES_;

   QDomDocument loadedDom_; ///< The DOM representation of the screenplay.
   QStringList characters_; ///< List of all characters in the screenplay.
   std::vector<QString> charByBlock_; ///< Vector of character names indexed by text block id.
   std::unordered_map<QString, std::pair<QTextCharFormat, QTextBlockFormat> > styleTypes_; ///< Stylings for different screenplay elements.
   QSize pageSize_; ///< Page size of the document.
   int dpiX_; ///< logical pixels in the X direction that measures to one inch.
   int dpiY_; ///< logical pixels in the Y direction that measures to one inch.
   bool isLoaded_; ///< Flag to indicate a screenplay has been loaded.
};

}

#endif // SCREENPLAY_H
