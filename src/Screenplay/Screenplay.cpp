#include "Screenplay.h"
#include "Characters/Characters.h"
#include "Voices/Voices.h"
#include <QTextCursor>

namespace FirstRead
{

const QString Screenplay::DEFAULT_ELEMENT_OWNER_ = "First Read Narrator";
const double Screenplay::OSF_TO_INCHES_ = 1 / 254.0;

Screenplay::Screenplay(int dpiX, int dpiY)
   : loadedDom_()
   , characters_()
   , charByBlock_()
   , styleTypes_()
   , pageSize_()
   , dpiX_(dpiX)
   , dpiY_(dpiY)
   , isLoaded_(false)
{

}

Screenplay::~Screenplay()
{

}

void Screenplay::Load(const QDomDocument& doc)
{
   loadedDom_ = doc;
   ConstructStylings(doc);
   GetCharacters(doc);
   isLoaded_ = true;
}

bool Screenplay::GetSettings(const QDomDocument& doc,
   int& pageHeight,
   int& pageWidth,
   int& topMargin,
   int& bottomMargin,
   int& leftMargin,
   int& rightMargin,
   double& linesPerInch)
{
   bool valid = false;
   QString pageHeightString;
   QString pageWidthString;
   QString topMarginString;
   QString bottomMarginString;
   QString leftMarginString;
   QString rightMarginString;
   QString linesPerInchString;

   QDomNodeList settings = doc.elementsByTagName("settings");
   for (int i = 0; i < settings.size(); i++)
   {
      QDomElement elmt = settings.at(i).toElement();

      if (elmt.hasAttribute("pageWidth"))
         pageWidthString = elmt.attribute("pageWidth");

      if (elmt.hasAttribute("pageHeight"))
         pageHeightString = elmt.attribute("pageHeight");

      if (elmt.hasAttribute("marginTop"))
         topMarginString = elmt.attribute("marginTop");

      if (elmt.hasAttribute("marginBottom"))
         bottomMarginString = elmt.attribute("marginBottom");

      if (elmt.hasAttribute("marginLeft"))
         leftMarginString = elmt.attribute("marginLeft");

      if (elmt.hasAttribute("marginRight"))
         rightMarginString = elmt.attribute("marginRight");

      if (elmt.hasAttribute("normalLinesPerInch"))
         linesPerInchString = elmt.attribute("normalLinesPerInch");
   }

   if (!pageWidthString.isEmpty() &&
      !pageHeightString.isEmpty() &&
      !topMarginString.isEmpty() &&
      !bottomMarginString.isEmpty() &&
      !leftMarginString.isEmpty() &&
      !rightMarginString.isEmpty() &&
      !linesPerInchString.isEmpty())
   {
      pageWidth = pageWidthString.toInt();
      pageHeight = pageHeightString.toInt();
      topMargin = topMarginString.toInt();
      bottomMargin = bottomMarginString.toInt();
      leftMargin = leftMarginString.toInt();
      rightMargin = rightMarginString.toInt();
      linesPerInch = linesPerInchString.toDouble();

      valid = true;
   }

   return valid;
}

void Screenplay::ConstructStylings(const QDomDocument& doc)
{
   int height;
   int width;
   int topMargin;
   int bottomMargin;
   int leftMargin;
   int rightMargin;
   double linesPerInch;
   if (GetSettings(doc, height, width, topMargin, bottomMargin, leftMargin, rightMargin, linesPerInch))
   {
      pageSize_ = QSize((width * OSF_TO_INCHES_ * dpiX_), (height * OSF_TO_INCHES_ * dpiY_));

      QDomNodeList styles = doc.elementsByTagName("styles");
      for (int i = 0; i < styles.count(); i++)
      {
         QDomNodeList children = styles.at(i).childNodes();
         for (int j = 0; j < children.count(); j++)
         {
            QDomElement elmt = children.at(j).toElement();
            QString name = elmt.attribute("name", "");
            if (!name.isEmpty())
            {
               QTextCharFormat charFormat;
               QString size = elmt.attribute("size", "");
               charFormat.setFontPointSize(!size.isEmpty() ? size.toDouble() : 12.0);
               bool allcaps = elmt.attribute("allCaps", "0").toInt();
               charFormat.setFontCapitalization(allcaps ? QFont::Capitalization::AllUppercase : QFont::Capitalization::MixedCase);
               bool bold = elmt.attribute("bold", "0").toInt();
               charFormat.setFontWeight(bold ? QFont::Weight::Bold : QFont::Weight::Normal);
               bool italic = elmt.attribute("italic", "0").toInt();
               charFormat.setFontItalic(italic);
               bool underline = elmt.attribute("underline", "0").toInt();
               charFormat.setUnderlineStyle(underline ? QTextCharFormat::UnderlineStyle::SingleUnderline : QTextCharFormat::UnderlineStyle::NoUnderline);

               QTextBlockFormat blockFormat;
               double spacebefore = elmt.attribute("spaceBefore", "0.0").toDouble();
               blockFormat.setTopMargin((spacebefore / linesPerInch) * dpiY_);
               int leftindent = elmt.attribute("leftIndent", "0").toInt() + leftMargin;
               blockFormat.setLeftMargin(leftindent * OSF_TO_INCHES_ * dpiX_);
               int rightindent = elmt.attribute("rightIndent", "0").toInt() + rightMargin;
               blockFormat.setRightMargin(rightindent * OSF_TO_INCHES_ * dpiX_);
               QString align = elmt.attribute("align", "");
               if (align == "right")
                  blockFormat.setAlignment(Qt::AlignRight);
               else if (align == "center")
                  blockFormat.setAlignment(Qt::AlignCenter);
               else
                  blockFormat.setAlignment(Qt::AlignLeft);

               styleTypes_.insert({ name, {charFormat, blockFormat} });
            }
         }
      }
   }
}

void Screenplay::GetCharacters(const QDomDocument& doc)
{
   characters_.clear();
   characters_.push_back(DEFAULT_ELEMENT_OWNER_);
   QDomNodeList chars = doc.elementsByTagName("character");
   int i = 0;
   while (i < chars.size())
   {
      characters_.push_back(chars.at(i).attributes().namedItem("name").nodeValue().toUpper());
      i++;
   }
}

void Screenplay::PopulateTextDocument(const QDomDocument& doc, QTextDocument& textDoc)
{
   QTextCursor* myCursor = new QTextCursor(&textDoc);
   QString lastCharacter;
   QString styleName;
   int i = 0;

   QDomNodeList viewableItems = doc.elementsByTagName("para");
   charByBlock_.resize(viewableItems.size());
   while (i < viewableItems.size())
   {
      QDomElement osfItemStyle = viewableItems.at(i).firstChildElement("style");

      if (!osfItemStyle.isNull())
      {
         styleName = osfItemStyle.attribute("baseStyleName", "");

         QString text = (styleName == "Parenthetical") ? "(" : "";
         QDomElement osfItemText = viewableItems.at(i).firstChildElement("text");
         while (!osfItemText.isNull())
         {
            text += osfItemText.firstChild().nodeValue();
            if (styleName == "Character")
            {
               QString charExt = osfItemText.firstChild().nodeValue().toUpper();
               // Remove character extensions (V.O.), (O.C.), (O.S.), (SUBTITLE).
               lastCharacter = charExt.mid(0, charExt.lastIndexOf("(")).trimmed();
            }
            if (styleName == "Parenthetical" || styleName == "Dialogue")
            {
               charByBlock_[i] = lastCharacter;
            }
            else
            {
               charByBlock_[i] = DEFAULT_ELEMENT_OWNER_;
            }

            osfItemText = osfItemText.nextSiblingElement("text");
         }

         text += (styleName == "Parenthetical") ? ")" : "";

         myCursor->setCharFormat(styleTypes_[styleName].first);
         myCursor->setBlockFormat(styleTypes_[styleName].second);
         myCursor->insertText(text);
      }
      if (++i != viewableItems.size())
      {
         myCursor->insertBlock();
      }
   }
}

const QStringList& Screenplay::GetCharacters() const
{
   return characters_;
}

void Screenplay::ToTextDocument(QTextDocument& textDoc)
{
   QFont f;
   f.setStyleHint(QFont::TypeWriter);
   f.setFamily("Courier Screenplay");
   f.setStyleStrategy(QFont::StyleStrategy::PreferAntialias);
   f.setPointSize(12);
   textDoc.setDefaultFont(f);
   textDoc.setPageSize(pageSize_);

   PopulateTextDocument(loadedDom_, textDoc);
}

QString Screenplay::GetCharacterByBlockId(unsigned int blockId) const
{
   return (blockId < charByBlock_.size()) ? charByBlock_[blockId] : "";
}

bool Screenplay::IsLoaded()
{
   return isLoaded_;
}

}