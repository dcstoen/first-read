#ifndef PAGE_GRAPHICS_VIEW_H
#define PAGE_GRAPHICS_VIEW_H

#include <QGraphicsView>

namespace FirstRead
{

/// @brief A graphics view representing a page with the ability to autoscale and maintain aspect ratio.
class PageGraphicsView : public QGraphicsView 
{
public:
   /// @brief Constructor.
   PageGraphicsView(QWidget* parent = 0);

   /// @brief  Destructor.
   virtual ~PageGraphicsView() = default;

   /// @name Disabled Operations
   /// @{
   PageGraphicsView(const PageGraphicsView&) = delete;
   PageGraphicsView& operator=(const PageGraphicsView&) = delete;
   /// @}

   /// @brief Scale the graphics view by a defined percentage.
   /// @param percent The percent to scale by. 
   /// @note Aspect ratio will remain the same.
   void ScaleByPercent(int percent);
};

}

#endif // PAGE_GRAPHICS_VIEW_H