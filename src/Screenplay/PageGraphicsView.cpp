#include "PageGraphicsView.h"
#include "PageGraphicsTextItem.h"

namespace FirstRead
{

PageGraphicsView::PageGraphicsView(QWidget* parent)
   : QGraphicsView(parent)
{
   setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
   setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
   setBackgroundBrush(QBrush(QColor(Qt::GlobalColor::darkGray)));
}

void PageGraphicsView::ScaleByPercent(int percent)
{
   qreal factor = percent / 100.0;
   resetTransform();
   scale(factor, factor);
}

}