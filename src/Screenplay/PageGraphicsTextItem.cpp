#include "PageGraphicsTextItem.h"
#include <QTextDocument>
#include <QTextCursor>
#include <QTextBlock>
#include <QPainter>

namespace FirstRead
{

PageGraphicsTextItem::PageGraphicsTextItem(QGraphicsItem* parent)
   : QGraphicsTextItem(parent)
{
   setDefaultTextColor(Qt::black);
}

PageGraphicsTextItem::PageGraphicsTextItem(const QString& text, QGraphicsItem* parent)
   : QGraphicsTextItem(text, parent)
{

}

PageGraphicsTextItem::~PageGraphicsTextItem()
{

}

void PageGraphicsTextItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
   painter->setBrush(Qt::white);
   painter->drawRect(boundingRect());
   QGraphicsTextItem::paint(painter, option, widget);
}

void PageGraphicsTextItem::ColorTextBlock(QColor color, int index)
{
   QTextCursor highlightCursor(document()->findBlockByNumber(index));
   QTextCursor cursor(document());

   QTextCharFormat plainFormat(highlightCursor.charFormat());
   QTextCharFormat colorFormat = plainFormat;
   colorFormat.setBackground(color);

   cursor.beginEditBlock();

   highlightCursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
   highlightCursor.mergeCharFormat(colorFormat);

   cursor.endEditBlock();
}

}