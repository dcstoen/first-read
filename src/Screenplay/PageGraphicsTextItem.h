#ifndef PAGE_GRAPHICS_TEXT_ITEM_H
#define PAGE_GRAPHICS_TEXT_ITEM_H

#include <QGraphicsTextItem>

namespace FirstRead
{

/// @brief QGraphicsTextItem speciailized to simulate 8.5 x 11 inch paper.
class PageGraphicsTextItem : public QGraphicsTextItem
{
public:
   /// @brief Constructor.
   /// @param parent The parent text item.
   PageGraphicsTextItem(QGraphicsItem* parent = nullptr);

   /// @brief Constructor.
   /// @param text The text to display.
   /// @param parent The parent text item.
   PageGraphicsTextItem(const QString& text, QGraphicsItem* parent = nullptr);

   /// @brief Destructor.
   virtual ~PageGraphicsTextItem();

   /// @brief Custom QGraphicsTextItem paint method which colors in the background white.
   virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

   /// @brief Paint a specific text block a given color.
   /// @param color The color to paint the text block.
   /// @param index The text block index to paint.
   void ColorTextBlock(QColor color, int index);
};

}

#endif // PAGE_GRAPHICS_TEXT_ITEM_H