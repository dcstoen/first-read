﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "version.h"
#include "Utils/VersionUtils.h"
#include "Screenplay/PageGraphicsTextItem.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QDomNodeList>
#include <QTextBlock>
#include <QGraphicsView>
#include <QGraphicsTextItem>
#include <QDesktopServices>
#include <QUrl>
#include <QMenu>
#include <QStyle>

namespace FirstRead
{

const double MainWindow::ZOOM_IN_AMOUNT_ = 1.25;
const int MainWindow::ZOOM_CEIL_ = 1000;
const int MainWindow::ZOOM_FLOOR_ = 1;

MainWindow::MainWindow(QWidget* parent)
   : QMainWindow(parent)
   , fileParse_()
   , screenplay_(logicalDpiX(), logicalDpiY())
   , screenplayDoc_()
   , fileTypesFilter_()
   , openedFile_()
   , engineName_()
   , playState_(PlayStates::STOPPED_)
   , activeVolume_(0.0)
   , currentTextBlock_(-1)
   , currentZoomScale_(100)
   , mute_(false)
   , ui(new Ui::FirstReadUi)
   , speech_(new QTextToSpeech(this))
   , voiceEditorUi_(new VoiceEditor(this))
   , characterBankUi_(new CharacterBank(this))
   , pageView_(new PageGraphicsView(this))
   , scene_(new QGraphicsScene(this))
   , page_(new PageGraphicsTextItem("page 1"))
   , engineMenu_(new QMenu(this))
   , engineGroup_(new QActionGroup(this))
   , zoomOutAction_(nullptr)
   , fitToViewAction_(nullptr)
   , zoomInAction_(nullptr)
   , jumpBeginningAction_(nullptr)
   , jumpPrevAction_(nullptr)
   , playAction_(nullptr)
   , jumpNextAction_(nullptr)
   , jumpEndAction_(nullptr)
   , stopAction_(nullptr)
   , muteAction_(nullptr)
{
   ui->setupUi(this);
   pageView_->setScene(scene_);
   ui->coreDisplay->layout()->addWidget(pageView_);

   SetupSupportedFileTypes();
   SetupMenu();
   SetupToolbar();
   SetupDocks();
}

void MainWindow::SetupSupportedFileTypes()
{
   // Get supporting files types only once during construction
   fileTypesFilter_ = "Screenplays ( ";
   const std::vector<std::string> supportedFileTypes = fileParse_.GetSupportedFileTypes();
   for (std::size_t i = 0; i < supportedFileTypes.size(); ++i)
   {
      fileTypesFilter_ += QString("*." + QString::fromStdString(supportedFileTypes[i]) + " ");
   }
   fileTypesFilter_ += ");; All files ( *.* )";
}

void MainWindow::SetupMenu()
{
   // Get supporting speech engines.
   for (auto& engine : QTextToSpeech::availableEngines())
   {
      QAction* act = engineMenu_->addAction(engine);
      act->setData(engine);
      act->setCheckable(true);
      act->setActionGroup(engineGroup_);
   }
   if (!engineMenu_->actions().isEmpty())
   {
      engineMenu_->actions().first()->setChecked(true);
      EngineChanged(engineMenu_->actions().first());
   }
   ui->speechEngine->setMenu(engineMenu_);

   ui->openMenuItem->setIcon(QIcon(":/icons/folder.svg"));
   ui->exportVoices->setIcon(QIcon(":/icons/export.svg"));
   ui->importVoices->setIcon(QIcon(":/icons/import.svg"));
   ui->speechEngine->setIcon(QIcon(":/icons/mic.svg"));
   ui->menuOptions->setIcon(QIcon(":/icons/cog.svg"));
   ui->viewHelp->setIcon(QIcon(":/icons/help.svg"));
   ui->aboutFirstRead->setIcon(QIcon(":/icons/info.svg"));
   ui->aboutQt->setIcon(style()->standardIcon(QStyle::StandardPixmap::SP_TitleBarMenuButton));

   QObject::connect(ui->openMenuItem, SIGNAL(triggered(bool)), this, SLOT(OpenFileClicked()));
   QObject::connect(ui->importVoices, SIGNAL(triggered(bool)), this, SLOT(ImportVoicesClicked()));
   QObject::connect(ui->exportVoices, SIGNAL(triggered(bool)), this, SLOT(ExportVoicesClicked()));
   QObject::connect(engineGroup_, SIGNAL(triggered(QAction*)), this, SLOT(EngineChanged(QAction*)));
   QObject::connect(ui->darkMode, SIGNAL(triggered(bool)), this, SLOT(DarkModeClicked(bool)));
   QObject::connect(ui->aboutFirstRead, SIGNAL(triggered(bool)), this, SLOT(AboutFirstReadClicked()));
   QObject::connect(ui->aboutQt, SIGNAL(triggered(bool)), this, SLOT(AboutQtClicked()));
   QObject::connect(ui->viewHelp, SIGNAL(triggered(bool)), this, SLOT(HelpClicked()));
}

void MainWindow::SetupToolbar()
{
   ui->toolBar->setIconSize(QSize(24, 24));

   zoomOutAction_ = ui->toolBar->addAction("Zoom out", this, SLOT(ZoomOut()));
   zoomOutAction_->setIcon(QIcon(":/icons/zoom-out.svg"));
   fitToViewAction_ = ui->toolBar->addAction("Fit to view", this, SLOT(FitToView()));
   fitToViewAction_->setIcon(QIcon(":/icons/zoom-fit.svg"));
   zoomInAction_ = ui->toolBar->addAction("Zoom in", this, SLOT(ZoomIn()));
   zoomInAction_->setIcon(QIcon(":/icons/zoom-in.svg"));

   ui->toolBar->addSeparator();
   jumpBeginningAction_ = ui->toolBar->addAction("Go to beginning", this, SLOT(ToBeginning()));
   jumpBeginningAction_->setIcon(QIcon(":/icons/jump-begin.svg"));
   jumpPrevAction_ = ui->toolBar->addAction("Skip previous", this, SLOT(SkipPrevious()));
   jumpPrevAction_->setIcon(QIcon(":/icons/jump-back.svg"));
   playAction_ = ui->toolBar->addAction("Play/Pause", this, SLOT(PlayPause()));
   playAction_->setIcon(QIcon(":/icons/play.svg"));
   playAction_->setToolTip("Play");
   jumpNextAction_ = ui->toolBar->addAction("Skip next", this, SLOT(SkipNext()));
   jumpNextAction_->setIcon(QIcon(":/icons/jump-next.svg"));
   jumpEndAction_ = ui->toolBar->addAction("Go to end", this, SLOT(ToEnd()));
   jumpEndAction_->setIcon(QIcon(":/icons/jump-end.svg"));
   stopAction_ = ui->toolBar->addAction("Stop", this, SLOT(Stop()));
   stopAction_->setIcon(QIcon(":/icons/stop.svg"));

   ui->toolBar->addSeparator();
   muteAction_ = ui->toolBar->addAction("Mute", this, SLOT(Mute()));
   muteAction_->setIcon(QIcon(":/icons/mute-on.svg"));
   muteAction_->setToolTip("Mute");

   ui->toolBar->setEnabled(false);
}

void MainWindow::SetupDocks()
{
   ui->characterDock->setWidget(characterBankUi_);
   ui->voiceEditorDock->setWidget(voiceEditorUi_);
   
   QObject::connect(voiceEditorUi_, SIGNAL(VoiceAdded(QString)), characterBankUi_, SLOT(AddVoice(QString)));
   QObject::connect(voiceEditorUi_, SIGNAL(VoiceRemoved(QString)), characterBankUi_, SLOT(RemoveVoice(QString)));
}

MainWindow::~MainWindow()
{
   delete ui;
   delete speech_;
   delete characterBankUi_;
   delete voiceEditorUi_;
   delete pageView_;
   delete scene_;
   delete engineMenu_;
   delete engineGroup_;
}

void MainWindow::OpenFileClicked()
{
   QString fileName = QFileDialog::getOpenFileName(this,
       tr("Open Screenplay"), "", tr(&fileTypesFilter_.toStdString()[0]));

   if (!fileName.isEmpty())
   {
      openedFile_ = fileName;
      if (fileParse_.ParserFile(openedFile_.toStdString(), screenplay_))
      {
         RefreshDisplay();
      }
      else
      {
         QMessageBox msgBox;
         msgBox.setText("Error: Failed to parse " + fileName);
         msgBox.exec();
      }
   }
}

void MainWindow::ImportVoicesClicked()
{
   QString defaultFile = openedFile_.mid(0, openedFile_.lastIndexOf('.')) + ".ftrd";
   QString fileName = QFileDialog::getOpenFileName(this,
      tr("Open Voices"), &defaultFile.toStdString()[0], tr("Voices ( *.ftrd)"));

   if (!fileName.isEmpty())
   {
      QFile myFile(fileName);
      if (myFile.open(QIODevice::ReadOnly))
      {
         QDataStream stream(&myFile);
         stream.setVersion(QDataStream::Qt_5_15);
         
         VersionUtils::VersionType version;
         VersionUtils::GetVersionNumberFromStream(version, stream);
         if (VersionUtils::CompatibleVoiceImportFile(version))
         {
            stream >> engineName_;
            voiceEditorUi_->ImportVoices(stream);
            characterBankUi_->ImportCharacterVoices(stream);

            myFile.close();

            for (auto& action : engineMenu_->actions())
            {
               if (action->data().toString() == engineName_)
               {
                  EngineChanged(action);
                  break;
               }
            }
         }
         else
         {
            QMessageBox msgBox;
            msgBox.setText("Error: Failed to import voices. Import file version " + 
               VersionUtils::GetVersionString(version) + " is incompatible with " + 
               VER_PRODUCTNAME_STR + " version " + VER_PRODUCTVERSION_STR);
            msgBox.exec();
         }
      }
   }
}

void MainWindow::ExportVoicesClicked()
{
   QString defaultFile = openedFile_.mid(0, openedFile_.lastIndexOf('.')) + ".ftrd";
   QString fileName = QFileDialog::getSaveFileName(this,
      tr("Save Voices"), &defaultFile.toStdString()[0], tr("Voices ( *.ftrd)"));

   if (!fileName.isEmpty())
   {
      QFile myFile(fileName);
      if (myFile.open(QIODevice::WriteOnly))
      {
         QDataStream stream(&myFile);
         stream.setVersion(QDataStream::Qt_5_15);

         VersionUtils::VersionType version;
         VersionUtils::GetVersionNumber(version);
         VersionUtils::StreamVersionNumber(version, stream);

         stream << engineName_;
         voiceEditorUi_->ExportVoices(stream);
         characterBankUi_->ExportCharacterVoices(stream);

         myFile.close();
      }
   }
}

void MainWindow::EngineChanged(QAction* action)
{
   if (action)
   {
      engineName_ = action->data().toString();
      
      QObject::disconnect(speech_, SIGNAL(stateChanged(QTextToSpeech::State)), this, SLOT(SpeechStateChange(QTextToSpeech::State)));
      delete speech_;
      speech_ = new QTextToSpeech(engineName_, this);
      QObject::connect(speech_, SIGNAL(stateChanged(QTextToSpeech::State)), this, SLOT(SpeechStateChange(QTextToSpeech::State)));

      voiceEditorUi_->EngineChanged(engineName_);
   }
}

void MainWindow::DarkModeClicked(bool clicked)
{
   if (clicked)
   {
      QFile file(":/Silverdark.qss");
      file.open(QFile::ReadOnly | QFile::Text);
      qApp->setStyleSheet(file.readAll());
   }
   else
   {
      QFile file(":/Silverlight.qss");
      file.open(QFile::ReadOnly | QFile::Text);
      qApp->setStyleSheet(file.readAll());
   }
}

void MainWindow::RefreshDisplay()
{
   // Populate the list of characters
   characterBankUi_->LoadCharacters(screenplay_);

   scene_->clear(); // scene_ deletes all items.
   page_ = new PageGraphicsTextItem();

   screenplay_.ToTextDocument(screenplayDoc_);
   page_->setDocument(&screenplayDoc_);
   
   scene_->addItem(page_); // scene_ takes ownership of all items.
   pageView_->setSceneRect(page_->boundingRect());

   ui->toolBar->setEnabled(screenplay_.IsLoaded());
   FitToView();
}

void MainWindow::ReadBlock(int block)
{
   currentTextBlock_ = block;
   QString readString = page_->document()->findBlockByNumber(currentTextBlock_).text();
   if (!readString.isEmpty())
   {
      QString voice = characterBankUi_->GetCharacterVoice(screenplay_.GetCharacterByBlockId(currentTextBlock_));
      const VoiceType* props = voiceEditorUi_->GetVoiceProperties(voice);
      if (props)
      {
         activeVolume_ = props->volume_;
         // Set locale before voice because setting locale resets voice selection.
         speech_->setLocale(props->locale_);
         speech_->setVoice(props->voice_);
         speech_->setVolume(mute_ ? 0.0 : props->volume_);
         speech_->setPitch(props->pitch_);
         speech_->setRate(props->rate_);

         speech_->say(readString);
      }
      else
      {
         QMessageBox msgBox;
         msgBox.setText("Read failed. Character \"" + 
            screenplay_.GetCharacterByBlockId(currentTextBlock_) +
            "\" is not assigned a valid voice.");
         msgBox.exec();
      }
   }
   else if (currentTextBlock_ < page_->document()->blockCount())
   {
      ReadBlock(currentTextBlock_ + 1);
   }
}

void MainWindow::ZoomIn()
{
   currentZoomScale_ = std::min<int>(std::ceil(currentZoomScale_ * ZOOM_IN_AMOUNT_), ZOOM_CEIL_);
   pageView_->ScaleByPercent(currentZoomScale_);
   SetZoomButtonsEnabled();
}

void MainWindow::ZoomOut()
{
   currentZoomScale_ = std::max<int>(std::floor(currentZoomScale_ * (2 - ZOOM_IN_AMOUNT_)), ZOOM_FLOOR_);
   pageView_->ScaleByPercent(currentZoomScale_);
   SetZoomButtonsEnabled();
}

void MainWindow::FitToView()
{
   const QGraphicsScene* currScene = pageView_->scene();
   if (currScene)
   {
      qreal currW = ui->coreDisplay->width();
      qreal sceneW = currScene->sceneRect().width();
      
      currentZoomScale_ = std::min<int>(std::max<int>((currW / sceneW) * 100, ZOOM_FLOOR_), ZOOM_CEIL_);
      pageView_->ScaleByPercent(currentZoomScale_);

      SetZoomButtonsEnabled();
   }
}

void MainWindow::SetZoomButtonsEnabled()
{
   bool screenplayLoaded = screenplay_.IsLoaded();

   zoomOutAction_->setEnabled(screenplayLoaded && currentZoomScale_ != ZOOM_FLOOR_);
   fitToViewAction_->setEnabled(screenplayLoaded);
   zoomInAction_->setEnabled(screenplayLoaded && currentZoomScale_ != ZOOM_CEIL_);
}

void MainWindow::ToBeginning()
{
   if (playState_ != PlayStates::STOPPED_)
   {
      playState_ = PlayStates::SEEKING_;
      Reset();
      ReadBlock(0);
   }
}

void MainWindow::SkipPrevious()
{
   if (playState_ != PlayStates::STOPPED_)
   {
      playState_ = PlayStates::SEEKING_;
      int currBlockCache = currentTextBlock_;
      Reset();
      ReadBlock(currBlockCache - 1);
   }
}

void MainWindow::PlayPause()
{
   switch (playState_)
   {
   case PlayStates::STOPPED_:
      ReadBlock(0);
      break;
   case PlayStates::PLAYING_:
   case PlayStates::SEEKING_:
      speech_->pause();
      break;
   case PlayStates::PAUSED_:
      if (speech_->state() == QTextToSpeech::Paused)
      {
         speech_->resume();
      }
      else
      {
         page_->ColorTextBlock(Qt::white, currentTextBlock_);
         ReadBlock(currentTextBlock_ + 1);
      }
      break;
   }
}

void MainWindow::SkipNext()
{
   if (playState_ != PlayStates::STOPPED_)
   {
      playState_ = PlayStates::SEEKING_;
      int currBlockCache = currentTextBlock_;
      Reset();
      ReadBlock(currBlockCache + 1);
   }
}

void MainWindow::ToEnd()
{
   if (playState_ != PlayStates::STOPPED_)
   {
      playState_ = PlayStates::SEEKING_;
      Reset();
      ReadBlock(page_->document()->blockCount() - 1);
   }
}

void MainWindow::Stop()
{
   playState_ = PlayStates::STOPPED_;
   Reset();
}

void MainWindow::Reset()
{
   speech_->stop();
   page_->ColorTextBlock(Qt::white, currentTextBlock_);
   currentTextBlock_ = -1;
}

void MainWindow::Mute()
{
   mute_ = !mute_;
   if (mute_)
   {
      speech_->setVolume(0);
      muteAction_->setIcon(QIcon(":/icons/mute-off.svg"));
      muteAction_->setToolTip("Unmute");
   }
   else
   {
      speech_->pause();
      speech_->setVolume(activeVolume_);
      speech_->resume();
      muteAction_->setIcon(QIcon(":/icons/mute-on.svg"));
      muteAction_->setToolTip("Mute");
   }
}

void MainWindow::SpeechStateChange(QTextToSpeech::State state)
{
   switch (state)
   {
   case QTextToSpeech::Ready:
      if (playState_ == PlayStates::PLAYING_)
      {
         page_->ColorTextBlock(Qt::white, currentTextBlock_);

         int nextBlock = currentTextBlock_ + 1;
         if (nextBlock >= 0 && nextBlock < page_->document()->blockCount())
         {
            ReadBlock(nextBlock);
         }
         else
         {
            Stop();
         }
      }
      else if (playState_ == PlayStates::STOPPED_)
      {
         playAction_->setIcon(QIcon(":/icons/play.svg"));
         playAction_->setToolTip("Play");
      }
      engineGroup_->setDisabled(false); // Enable ability to change engines while not speaking.
      break;
   case QTextToSpeech::Speaking:
      playState_ = PlayStates::PLAYING_;
      page_->ColorTextBlock(Qt::yellow, currentTextBlock_);
      engineGroup_->setDisabled(true); // Disable ability to change engines while speaking.
      playAction_->setIcon(QIcon(":/icons/pause.svg"));
      playAction_->setToolTip("Pause");
      break;
   case QTextToSpeech::Paused:
      playState_ = PlayStates::PAUSED_;
      playAction_->setIcon(QIcon(":/icons/play.svg"));
      playAction_->setToolTip("Play");
      break;
   case QTextToSpeech::BackendError:
      page_->ColorTextBlock(Qt::red, currentTextBlock_);
      break;
   }
}

void MainWindow::AboutFirstReadClicked()
{
   QString aboutText =
      QString(VER_PRODUCTNAME_STR) + '\n' +
      QString("Version ") + QString(VER_PRODUCTVERSION_STR) + '\n' +
      QString(VER_LEGALCOPYRIGHT_STR) + '\n' + '\n' +
      QString("The program is provided AS IS with NO WARRANTY OF ANY KIND.");
   QMessageBox::about(this, "About First Read", aboutText);
}

void MainWindow::AboutQtClicked()
{
   QMessageBox::aboutQt(this, "About Qt");
}

void MainWindow::HelpClicked()
{
   QUrl url = QUrl::fromLocalFile(QApplication::applicationDirPath() + "/../doc/html/index.html");
   if (url.isValid())
   {
      QDesktopServices::openUrl(url);
   }
}

}