#ifndef VOICE_TYPE_H
#define VOICE_TYPE_H

#include <QVoice>
#include <QLocale>

namespace FirstRead
{

/// @brief Structure of properties that define a unique voice.
struct VoiceType
{
   /// @brief Constructor.
   VoiceType();

   QVoice voice_; ///< The voice being used.
   QLocale locale_; ///< The locale being used.
   double volume_; ///< The volume of the voice.
   double pitch_; ///< The pitch of the voice.
   double rate_; ///< The rate of the voice.
};

}

#endif // VOICE_TYPE_H