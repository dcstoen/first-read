#include "VoiceType.h"

namespace FirstRead
{

VoiceType::VoiceType()
   : voice_()
   , locale_()
   , volume_(0.5)
   , pitch_(0.0)
   , rate_(0.0)
{

}

}