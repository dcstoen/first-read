#include "Voices.h"

namespace FirstRead
{

Voices::Voices()
   : voices_()
{

}

Voices::~Voices()
{

}

bool Voices::CreateVoice(const QString& name, const VoiceType& properties)
{
   return !name.isEmpty() && voices_.insert_or_assign(name, properties).second;
}

bool Voices::DeleteVoice(const QString& name)
{
   return voices_.erase(name);
}

const VoiceType* Voices::GetVoiceProperties(const QString& name)
{
   auto it = voices_.find(name);
   if (it == voices_.end())
   {
      return nullptr;
   }
   return &(it->second);
}

const Voices::VoicesType& Voices::GetVoices()
{
   return voices_;
}

}