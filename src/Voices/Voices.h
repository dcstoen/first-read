#ifndef VOICES_H
#define VOICES_H

#include <QTextToSpeech>
#include <map>
#include <set>
#include <string>
#include "VoiceType.h"

namespace FirstRead
{

/// @brief All saved voices with their corresponding attributes.
class Voices
{
public:
   /// Convenience typedef of voice name to voice properties.
   typedef std::map<QString, VoiceType> VoicesType;

   /// @brief Default constructor.
   Voices();

   /// @brief Destructor.
   virtual ~Voices();

   /// @brief Get a voice by name. 
   /// @param name The name of the voice to retrieve.
   /// @return The voice properties for a given voice name.
   /// @note If no matching voice properties for a given voice name is found
   /// then a null pointer is returned.
   const VoiceType* GetVoiceProperties(const QString& name);

   /// @brief Gets all available voices.
   /// @return All available voice.
   const VoicesType& GetVoices();

   /// @brief Add a voice to the database.
   /// @param name The name of the voice being added. Must be unique among all saved voices.
   /// @param properties The voice properties of the voice being added.
   /// @return True if the voice was created.
   bool CreateVoice(const QString& name, const VoiceType& properties);

   /// @brief Removes a voice from the database.
   /// @param name The name of the voice to remove.
   /// @return True if the voice was removed.
   bool DeleteVoice(const QString& name);

protected:
   VoicesType voices_; ///< All saved voices.
};

}

#endif // VOICES_H
