#include "VoiceEditor.h"
#include "ui_VoiceEditor.h"
#include <QMessageBox>

namespace FirstRead
{

VoiceEditor::VoiceEditor(QWidget *parent)
   : QWidget(parent)
   , ui(new Ui::VoiceEditorDock)
   , voices_()
   , speech_(nullptr)
   , currentVoices_()
{
   ui->setupUi(this);

   // Setup signal and slots
   QObject::connect(ui->readButton, SIGNAL(clicked(bool)), this, SLOT(ReadClicked()));
   QObject::connect(ui->volumeSlider, SIGNAL(valueChanged(int)), this, SLOT(VolumeChanged(int)));
   QObject::connect(ui->pitchSlider, SIGNAL(valueChanged(int)), this, SLOT(PitchChanged(int)));
   QObject::connect(ui->rateSlider, SIGNAL(valueChanged(int)), this, SLOT(RateChanged(int)));
   QObject::connect(ui->saveAsButton, SIGNAL(clicked(bool)), this, SLOT(SaveClicked()));
   QObject::connect(ui->deleteButton, SIGNAL(clicked(bool)), this, SLOT(DeleteClicked()));

   ui->deleteButton->setIcon(QIcon(":/icons/red-x.svg"));
   ui->saveAsButton->setIcon(QIcon(":/icons/green-check.svg"));
   ui->deleteButton->setFlat(true);
   ui->saveAsButton->setFlat(true);
}

VoiceEditor::~VoiceEditor()
{
   delete ui;
   delete speech_;
}

void VoiceEditor::ExportVoices(QDataStream& os)
{
   os << static_cast<quint32>(voices_.GetVoices().size());
   for (auto& voice : voices_.GetVoices())
   {
      os << voice.first
         << voice.second.voice_.name()
         << voice.second.voice_.gender()
         << voice.second.voice_.age()
         << voice.second.locale_.language()
         << voice.second.locale_.script()
         << voice.second.locale_.country()
         << voice.second.volume_
         << voice.second.pitch_
         << voice.second.rate_;
   }
}

void VoiceEditor::ImportVoices(QDataStream& is)
{
   QString userName;
   QString voiceName;
   QVoice::Gender gender;
   QVoice::Age age;
   QLocale::Language language;
   QLocale::Script script;
   QLocale::Country country;

   quint32 size;
   is >> size;
   for (quint32 i = 0; i < size; ++i)
   {
      VoiceType voice;
      is >> userName
         >> voiceName
         >> gender
         >> age
         >> language
         >> script
         >> country
         >> voice.volume_
         >> voice.pitch_
         >> voice.rate_;

      voice.voice_ = FindVoice(voiceName, gender, age);
      voice.locale_ = QLocale(language, script, country);
      if (voices_.CreateVoice(userName, voice))
      {
         emit VoiceAdded(userName);
      }
   }
   LoadSavedVoices();
}

const VoiceType* VoiceEditor::GetVoiceProperties(const QString& name)
{
   return voices_.GetVoiceProperties(name);
}

void VoiceEditor::EngineChanged(QString engine)
{
   delete speech_;
   speech_ = new QTextToSpeech(engine, this);
   
   QObject::disconnect(ui->languageCB, SIGNAL(currentIndexChanged(int)), this, SLOT(LanguageSelected(int)));
   ui->languageCB->clear();
   QLocale current = speech_->locale();
   for (auto& locale : speech_->availableLocales()) 
   {
      ui->languageCB->addItem(GetLocaleString(locale), QVariant(locale));
      if (locale.name() == current.name())
      { 
         current = locale;
      }
   }
   RateChanged(ui->rateSlider->value());
   PitchChanged(ui->pitchSlider->value());
   VolumeChanged(ui->volumeSlider->value());

   QObject::connect(speech_, SIGNAL(stateChanged(QTextToSpeech::State)), this, SLOT(StateChanged(QTextToSpeech::State)));
   QObject::connect(speech_, SIGNAL(localeChanged(const QLocale&)), this, SLOT(LocaleChanged(const QLocale&)));
   QObject::connect(ui->pitchSlider, SIGNAL(valueChanged(int)), this, SLOT(PitchChanged(int)));
   QObject::connect(ui->rateSlider, SIGNAL(valueChanged(int)), this, SLOT(RateChanged(int)));
   QObject::connect(ui->volumeSlider, SIGNAL(valueChanged(int)), this, SLOT(VolumeChanged(int)));
   QObject::connect(ui->savedVoicesCB, SIGNAL(currentIndexChanged(int)), this, SLOT(SavedVoiceSelectionChanged(int)));
   QObject::connect(ui->languageCB, SIGNAL(currentIndexChanged(int)), this, SLOT(LanguageSelected(int)));
   LocaleChanged(current);
}

void VoiceEditor::LocaleChanged(const QLocale& locale)
{
   QVariant localeVariant(locale);
   ui->languageCB->setCurrentIndex(ui->languageCB->findData(localeVariant));

   QObject::disconnect(ui->voiceCB, SIGNAL(currentIndexChanged(int)), this, SLOT(VoiceSelected(int)));
   ui->voiceCB->clear();

   currentVoices_ = speech_->availableVoices();
   QVoice currentVoice = speech_->voice();
   for (auto& voice : currentVoices_)
   {
      ui->voiceCB->addItem(GetVoiceString(voice));
      if (voice.name() == currentVoice.name())
      {
         ui->voiceCB->setCurrentIndex(ui->voiceCB->count() - 1);
      }
   }
   QObject::connect(ui->voiceCB, SIGNAL(currentIndexChanged(int)), this, SLOT(VoiceSelected(int)));
}

void VoiceEditor::SavedVoiceSelectionChanged(int index)
{
   QString name = ui->savedVoicesCB->itemText(index);
   const VoiceType* voice = voices_.GetVoiceProperties(name);
   if (voice)
   {
      ui->languageCB->setCurrentIndex(ui->languageCB->findText(GetLocaleString(voice->locale_)));
      ui->voiceCB->setCurrentIndex(ui->voiceCB->findText(GetVoiceString(voice->voice_)));
      ui->volumeSlider->setValue(voice->volume_ * 100.0);
      ui->rateSlider->setValue(voice->rate_ * 10.0);
      ui->pitchSlider->setValue(voice->pitch_ * 10.0);
   }
}

void VoiceEditor::StateChanged(QTextToSpeech::State state)
{
   ui->readButton->setEnabled(state == QTextToSpeech::State::Ready);
}

void VoiceEditor::ReadClicked()
{
   speech_->say(ui->sampleText->toPlainText());
}

void VoiceEditor::VolumeChanged(int volume)
{
   speech_->setVolume(volume / 100.0);
}

void VoiceEditor::PitchChanged(int pitch)
{
   speech_->setPitch(pitch / 10.0);
}

void VoiceEditor::RateChanged(int rate)
{
   speech_->setRate(rate / 10.0);
}

void VoiceEditor::LanguageSelected(int language)
{
   speech_->setLocale(ui->languageCB->itemData(language).toLocale());
}

void VoiceEditor::VoiceSelected(int index)
{
   speech_->setVoice(currentVoices_.at(index));
}

void VoiceEditor::SaveClicked()
{
   QString name = ui->saveAsLine->text();
   if (!name.isEmpty())
   {
      QString command = voices_.GetVoices().count(name) > 0 ? "Override" : "Create";
      QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Save", command + " voice: \"" + name + "\"?",
         QMessageBox::Yes | QMessageBox::No);
      if (reply == QMessageBox::Yes) 
      {
         VoiceType properties;
         properties.voice_ = speech_->voice();
         properties.locale_ = speech_->locale();
         properties.volume_ = speech_->volume();
         properties.pitch_ = speech_->pitch();
         properties.rate_ = speech_->rate();
         if (voices_.CreateVoice(name, properties))
         {
            emit VoiceAdded(name);
            LoadSavedVoices();
            ui->savedVoicesCB->setCurrentIndex(ui->savedVoicesCB->findText(name));
         }
      }
   }
}

void VoiceEditor::DeleteClicked()
{
   QString name = ui->savedVoicesCB->currentText();
   if (!name.isEmpty())
   {
      QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Delete", "Delete voice: \"" + name + "\"?",
         QMessageBox::Yes | QMessageBox::No);
      if (reply == QMessageBox::Yes)
      {
         if (voices_.DeleteVoice(name))
         {
            emit VoiceRemoved(name);
            LoadSavedVoices();
         }
      }
   }
}

void VoiceEditor::LoadSavedVoices()
{
   ui->savedVoicesCB->clear();
   for (auto& voice : voices_.GetVoices())
   {
      ui->savedVoicesCB->addItem(voice.first);
   }
}

QVoice VoiceEditor::FindVoice(const QString& name, const QVoice::Gender& gender, const QVoice::Age& age)
{
   for (auto& voice : speech_->availableVoices())
   {
      if (voice.name() == name && voice.gender() == gender && voice.age() == age)
      {
         return voice;
      }
   }

   return QVoice();
}

QString VoiceEditor::GetLocaleString(const QLocale& locale)
{
   QString name(QString("%1 (%2)")
      .arg(QLocale::languageToString(locale.language()))
      .arg(QLocale::countryToString(locale.country())));

   return name;
}

QString VoiceEditor::GetVoiceString(const QVoice& voice)
{
   QString name(QString("%1 - %2 - %3").arg(voice.name())
      .arg(QVoice::genderName(voice.gender()))
      .arg(QVoice::ageName(voice.age())));

   return name;
}

}