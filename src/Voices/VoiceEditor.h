#ifndef VOICE_EDITOR_H
#define VOICE_EDITOR_H

#include <qwidget.h>
#include "Voices.h"

namespace Ui
{
   class VoiceEditorDock;
}

namespace FirstRead
{

/// @brief UI logic for adding and modifying voices.
class VoiceEditor : public QWidget
{
   Q_OBJECT

public:
   /// @brief Constructor
   /// @param parent Pointer to the parent of this widget.
   explicit VoiceEditor(QWidget *parent = 0);

   /// @brief Destructor.
   virtual ~VoiceEditor();

   /// @brief Get a voice by name. 
   /// @param name The name of the voice to retrieve.
   /// @return The voice properties for a given voice name.
   /// @note If no matching voice properties for a given voice name is found
   /// then a null pointer is returned.
   const VoiceType* GetVoiceProperties(const QString& name);

   /// @brief Serializes the current saved voices.
   /// @param os Output serialized data stream.
   void ExportVoices(QDataStream& os);

   /// @brief Generates saved voices from serializes data.
   /// @param is Input serialized data stream.
   void ImportVoices(QDataStream& is);

public slots:
   /// @brief Slot for when the user selects a different speech engine.
   /// Selecting a different engine causes the available voice selection to refresh.
   /// @param engine Name of the engine selected.
   void EngineChanged(QString engine);

   /// @brief Slot for when the user selects a different saved voice.
   /// @param index The index of the saved voice that was selected.
   void SavedVoiceSelectionChanged(int index);

   /// @brief Slot for when the user selects a different locale for their voice.
   /// @param locale Locale selected by the user.
   void LocaleChanged(const QLocale& locale);

   /// @brief Slot for when the active text to speech object changes state.
   /// @param state The state being changed to.
   void StateChanged(QTextToSpeech::State state);

   /// @brief Slot for when the user selects a different language for their voice.
   /// @param language The index of the language selected in the combobox.
   void LanguageSelected(int language);

   /// @brief Slot for when a user modifies the voice volumn.
   /// @param volume New volume level from (0,1).
   void VolumeChanged(int volume);

   /// @brief Slot for when a user modifies the voice pitch.
   /// @param pitch New pitch level from (-1,1).
   void PitchChanged(int pitch);

   /// @brief Slot for when a user modifies the voice speech rate.
   /// @param rate New rate level from (-1,1).
   void RateChanged(int rate);

   /// @brief Slot for when a user selects a different voice actor to use.
   /// @param index The index of the voice selected in the combobox. 
   void VoiceSelected(int index);

   /// @brief Slot for when the user clicks "read."
   void ReadClicked();

   /// @brief Slot for when the user saves the current voice parameters.
   void SaveClicked();

   /// @brief Slot for when the user deletes a previously saved voice.
   void DeleteClicked();

signals:
   /// @brief Signal emitted when a voice is added to the voice database.
   /// @param name The name of the voice being added.
   void VoiceAdded(QString name);

   /// @brief Signal emitted when a voice is removed from the voice database.
   /// @param name The name of the voice being removed. 
   void VoiceRemoved(QString name);

private:
   /// @brief Loads all saved voices to the voice selection combobox.
   void LoadSavedVoices();

   /// @brief Find a voice from voice properties.
   /// @param name The name of the voice to find.
   /// @param gender The gender of the voice to find.
   /// @param age The age of the voice to find.
   /// @return The voice matching all given properties.
   QVoice FindVoice(const QString& name, const QVoice::Gender& gender, const QVoice::Age& age);

   /// @brief Creates and returns a string representing a locale.
   /// @param locale The locale to construct the string from.
   /// @return The constructed locale string.
   QString GetLocaleString(const QLocale& locale);

   /// @brief Creates and returns a string representing a voice.
   /// @param voice The voice to construct the string from.
   /// @return The constructed voice string.
   QString GetVoiceString(const QVoice& voice);

   Ui::VoiceEditorDock* ui; ///< UI
   QTextToSpeech* speech_; ///< Text to speech object for reading back sample text.
   Voices voices_; ///< Pointer to the voices database that stores all currently saved voices.
   QVector<QVoice> currentVoices_; ///< All current voices available with the selected engine.
};

}

#endif // VOICE_EDITOR_H
