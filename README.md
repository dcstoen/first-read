First Read
===

# Overview
First Read is a text to speech program specifically designed for reading screenplays.
Users can customize voices for each character in their screenplay and First Read will
read the screenplay back using the specified voices.

## Customize Voices
Text to speech is accomplished using Qt's [QTextToSpeech][]. Therefore, any TTS engine/voices 
installed locally should be available to First Read. Voice property modifications include 
changing speaking volume, raising or lowering pitch, and increasing or decreasing the rate 
of speech.

Once all desired adjustments have been made to a particular voice the user can save the 
voice to be made available for character assignment. Any number of voices can be saved
and any number of characters can use the same voice if desired.

## Assign Characters
Upon loading a screenplay file, users can assign voices to each character. First Read 
will use that character's assigned voice whenever that character's dialog is spoken 
(including parentheticals).

## Import and Export Voices
Voice configurations and character assignments can be loaded from a previously saved `ftrd` 
file. Importing and exporting voices can be accomplished using First Read's file menu.

## Supported Files
First Read works natively with `osf` (Open Screenplay Format) files. However, the program
is capable of reading a variety of screenplay file types. Below is a complete list of all 
supported file types.

- `osf` (Open Screenplay Format)
- `xml` (Assumed to be OSF formatted)

---

# Screenshots
Below are screenshots of First Read in action with both the default *Silverlight* theme
and the dark mode *Silverdark* theme.

The images were captured using the sample script provided with First Read's documentation.

![First Read with Silverlight theme.](/doc/imgs/Sample.PNG "Silverlight image")
![First Read with Silverdark theme.](/doc/imgs/SampleDarkMode.PNG "Silverdark image")

---

# Building the Executable
First Read uses CMake to simplify multi-IDE development support. Running CMake on the top
level directory's `CMakeLists` allows developers to generate their project and compile with
their preferred C++ compiler. That being said, **only Windows 10 running Visual Studio 2019 
has been tested**.

## Required Dependencies
First Read uses the following dependencies for development.

Dependency | Tested Version | Notes
-----------|----------------|------
CMake      | 3.18.0         | Assumed to be added to your `PATH` environment variable.
Qt         | 5.15.0         | Assumed default install path of `C:\Qt`.
Boost      | 1.73.0         | Assumed to include the following: `boost::unit_test_framework`.
Doxygen    | 1.8.18         | Assumed to be added to your `PATH` environment variable.

Other versions of these tools may work but they have not been tested.

## Windows Visual Studio Developers
If you are a Windows developer using Visual Studio follow the steps below. 
- Navigate to `/build`
- Open `GenerateSolution.cmd`
- Verify Visual Studio version and Qt install path
- Run `GenerateSolution.cmd`
- Open and build the resulting `FirstRead.sln` file

---

# Licensing and Acknowledgments
First Read uses the MIT license. See First Read's [Licensing Agreement][FirstReadLicense]
for more details.

First Read would not be made possible without the tools provided by various third parties.
The following are the licensing agreements of all technologies used inside First Read.

Technology            | License
----------------------|---------
[Qt 5.15.0][Qt]       | [LGPL v3][QtLicense]
[Boost 1.73.0][Boost] | [BSL v1.0][BoostLicense]


[FirstReadLicense]: ./LICENSE.md
[Qt]: https://doc.qt.io/qt-5/
[QTextToSpeech]: https://doc.qt.io/qt-5/qtexttospeech.html
[QtLicense]: https://doc.qt.io/qt-5.15/lgpl.html
[Boost]: https://www.boost.org/doc/libs/1_73_0/
[BoostLicense]: https://www.boost.org/LICENSE_1_0.txt